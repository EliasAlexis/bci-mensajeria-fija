import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthRoutingModule } from './auth-routing.module';
import { LoginComponent } from './login/login.component';
import { ObtenerPassComponent } from './obtener-pass/obtener-pass.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RestorePassComponent } from './restore-pass/restore-pass.component';
import { ActivateUserComponent } from './activate-user/activate-user.component';
import { AuthComponent } from './auth.component'
import { NgxMaskModule, IConfig } from 'ngx-mask';

import { ChileanMaskDirective } from './services/directives'
export const options: Partial<IConfig> | (() => Partial<IConfig>) = {}

@NgModule({
  declarations: [LoginComponent, ObtenerPassComponent, RestorePassComponent, ActivateUserComponent, AuthComponent, ChileanMaskDirective],
  imports: [
    CommonModule,
    AuthRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgxMaskModule.forRoot(options),
  ]
})
export class AuthModule { }
