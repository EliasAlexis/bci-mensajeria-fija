import { Component, OnInit } from '@angular/core';
import { ObtenerPass } from '../models';
import { AuthService } from '../services/auth.service';
import { ToastrService } from 'ngx-toastr';
import { MetaTitleService } from '../../services/meta-title.service';

@Component({
  selector: 'app-obtener-pass',
  templateUrl: './obtener-pass.component.html',
  styleUrls: ['./obtener-pass.component.scss']
})
export class ObtenerPassComponent implements OnInit {

  obtenerPass : ObtenerPass = new ObtenerPass (); 
  isLoading: boolean = false;

  constructor(
    private authService:AuthService,
    private toastrService : ToastrService,
    private metaTitleService: MetaTitleService,
    ) {
    this.metaTitleService.updateTitle();
    }

  ngOnInit() {
  }

  obtenerPasword() {
    this.isLoading = true;
    this.authService.obtenerPasword(this.obtenerPass)
    .subscribe(
      response => {
        this.isLoading = false;
        // console.log("ok");
        // console.log(response);
        this.toastrService.success('Las indicaciones para restablecer su contraseña fueron enviadas al correo electrónico', 'Datos ingresado correctamente',{timeOut: 5000 });
      },
      error => {
        this.isLoading = false;
        // console.log("error");
        // console.log(error);
        this.toastrService.error('', 'Correo no registrado');
      }
    )
    // console.log(this.obtenerPass)
  }
 
}
