import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, CanLoad, UrlSegment, Router, CanActivate, Route } from '@angular/router';
import { Observable, of } from 'rxjs';
import { environment } from '../../environments/environment';
import { map, catchError } from 'rxjs/operators'

@Injectable()
export class RouteGuard implements CanActivate, CanLoad {
    constructor(
        public router: Router,
        private http: HttpClient,
    ) { }

    canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
        return this.http.post(environment.server_ip + 'render/', { segment: route.data.segment })
            .pipe(
                map(() => true),
                catchError(() => of(false))
            )
    }

    canLoad(route: Route, segments: UrlSegment[]): Observable<boolean>{
        return this.http.post(environment.server_ip + 'render/', { segment: route.data.segment })
            .pipe(
                map(() => true),
                catchError(() => of(false))
            )
    }
}