import { Injectable, ErrorHandler } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';

/**
 * Handles general XHR errors
 */
@Injectable({
  providedIn: 'root'
})
export class XhrErrorHandlerService implements ErrorHandler {

  public handleError (error: HttpErrorResponse | any) {
    let errMsg: any = {};
    if (error instanceof HttpErrorResponse && (error.status !== 0) && (error.status !== 500)) {
      errMsg = {
        errMsg: error.error,
        status: error.status
      };
    } else {
      errMsg = {
        errMsg: {
            failure: ['Algo ha salido mal, inténtalo de nuevo']
          },
        status: error.status
      };
    }
    return throwError(errMsg);
  }
}
