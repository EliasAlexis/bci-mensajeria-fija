import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsersComponent } from "./users.component";
import { SolicitudOtComponent } from '../layouts/shared/solicitud-ot/solicitud-ot.component';
import { ListOtComponent } from './ot/list-ot/list-ot.component';
import { DashboardComponent } from '../layouts/shared/dashboard/dashboard.component';
import { TrackingComponent } from '../commons/tracking/tracking.component';
import { UserResolver } from "./user.resolve";
import { BulkComponent } from '../layouts/shared/solicitud-ot/bulk/bulk.component';

const routes: Routes = [
  { 
    path: '',
    component: UsersComponent,
    children: [
        {
          path: 'dash',
          component: DashboardComponent,
        },{
          path: 'solicitudot/new/bulk',
          component: BulkComponent,
          data: {
            title: 'Carga Masiva'
          }
        },{
          path :'solicitudot/:id',
          component : SolicitudOtComponent,
          resolve: { wo: UserResolver },
          data: { 
            group: 'dXNlcnM=',
            title: 'Nueva Solicitud'
          }
        },{
          path :'wolist',
          component : ListOtComponent,
          data: {
            title: 'Listado de solicitudes'
          }
        },{
          path: 'wolist/tracking',
          component: TrackingComponent,
          data: {
            title: 'Seguimiento'
          }
        } 
      ]
    }
  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [
    UserResolver,
  ]
})
export class AdminRoutingModule { }
  