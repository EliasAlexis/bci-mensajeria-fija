import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http'
import {environment} from '../../environments/environment'
import { XhrErrorHandlerService } from "../services/xhr-error-handler.service";
import { catchError } from 'rxjs/operators';

@Injectable()
export class ServicesService {

  constructor(
    private http : HttpClient,
    private xhrErrorHandlerService: XhrErrorHandlerService,
  ) { }

  public listService(){
   return this.http.get(
      environment.server_ip+'settings/services/'
    )
  }

  public saveOtService( formOt: any ){
    return this.http.post(
      environment.server_ip+'wo/', formOt
    )
  }

  public listOtService(consulta){
    // console.log( {params:consulta});
    return this.http.get(
      environment.server_ip +'wo/', {params:consulta} 
    )
  }

  public wiewListOtService(id: Number){
    return this.http.get(
      environment.server_ip + 'wo/'+ id+'/'
    ).pipe(
      catchError(this.xhrErrorHandlerService.handleError)
    )
  }

  public replaceOt(id,dataRepalce){
    return this.http.put(
      environment.server_ip + 'wo/'+ id+'/', dataRepalce
    )
  }
  
  public listContacService(params){
    return this.http.get(
      environment.server_ip + 'autocomplete/',
      { params: params }
    )
  }

  public listProductsService(){
    return this.http.get(
      environment.server_ip+'settings/products/'
    )
  }

  public extra_fields(){
    return this.http.get(
      environment.server_ip+'settings/extra_fields/?company=12'
    )
  }

}
