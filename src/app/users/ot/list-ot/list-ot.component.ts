import { Component, OnInit,ViewChild,ElementRef,Renderer2} from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { ServicesService } from  '../../services.service';
import { DownloadService } from '../../../services/download.service';
import { MetaTitleService } from '../../../services/meta-title.service';

@Component({
  selector: 'app-list-ot',
  templateUrl: './list-ot.component.html',
  styleUrls: ['./list-ot.component.scss']
})
export class ListOtComponent implements OnInit {

  @ViewChild('TableListOt') tableListOt : ElementRef; 
  
  constructor(
    private activatedRoute : ActivatedRoute,
    private router : Router,
    private usersService : ServicesService,
    private renderer: Renderer2,
    private downloadService: DownloadService,
    private metaTitleService: MetaTitleService,
  ) {
    this.metaTitleService.updateTitle();
  }

  dtOptions: any = {
    dom: 'Bfrtip',
    serverSide: true,
    processing: true,
    language: {
      url: '../../../assets/languages/Spanish.json'
    },
    order: [[0, "desc"]],
    buttons: {
      buttons:[
        {
           text: 'Excel',
           key: '2',
           className: 'btn btn-success btn-sm text-white',
           action: (e, dt, node, config) => {
             let dtparams = dt.ajax.params()
             let params = {
               report_type: 'excel',
               search: dtparams.search.value
             }
             this.downloadService.downloadReport('wo/reports/', params)
               .subscribe(
                 res => {
                   this.downloadFile('xlsx', res)
                 }
               )
           }
         },{
          text: 'PDF',
          key: '2',
          className: 'btn btn-danger btn-sm text-white',
          action: (e, dt, node, config) => {
            let dtparams = dt.ajax.params()
            let params = {
              report_type: 'pdf',
              search: dtparams.search.value
            }
            this.downloadService.downloadReport('wo/reports/', params)
              .subscribe(
                res => {
                  this.downloadFile('pdf', res)
                }
              )
          }
        },
       ]
    },
    columns: [
      {
        title: 'N° OT',
        data: 'barcode'
      },
      {
        title: 'Fecha emisión',
        data: 'created_at',
        render: function (data: any, type: any, full: any) {
          if(data){
            return data[0].toUpperCase() + data.slice(1);
          }else{
            return data;
          }
        }
      },
      {
        title: 'Nombre Emisor',
        data: 'sender_full_name',
        render: function (data: any, type: any, full: any) {
          if(data){
            return data[0].toUpperCase() + data.slice(1);
          }else{
            return data;
          }
        }
      },
      {
        title: 'Dirección origen',
        data: 'sender_address'
      },
      {
        title: 'Nombre receptor',
        data: 'receiver_full_name'
      },
      {
        title: 'Dirección destino',
        data: 'receiver_address'
      },
      {
        title: 'Último estado',
        data: 'current_step.name'
      },
      {
        title: 'Detalle',
        data: 'id',
        orderable: false,
        className: 'text-center',
        render: function (data: any, type: any, full: any) {
          return `<button type="button" class="btn btn-info btn-sm" view-wo-detail="${data}">
                    <i class="fas fa-edit" view-wo-detail="${data}"></i>
                  </button>`;
        }
      },
      {
        title: 'Seguimiento',
        data: 'barcode',
        orderable: false,
        className: 'text-center',
        render: function (data: any, type: any, full: any) {
          return `<button type="button" class="btn btn-info btn-sm" view-wo-id="${data}">
                    <i class="fas fa-motorcycle" view-wo-id="${data}"></i>
                  </button>`;
        }
      },
    ],
    ajax : (data : any, callback ) => {
      let order = data.order[0];
      let consulta :any = {
        search: data.search.value,
        offset: data.start,
        limit : data.length,
        ordering: (order.dir == 'desc'?'-':'') + data.columns[order.column].data
      };
      
      this.usersService.listOtService(consulta)
      .subscribe(
        (response: any) => {
          callback({
            recordsTotal : response.count,
            recordsFiltered : response.count,
            data : response.results
          });
        },
          error => {
            // console.log(error);
        }
      )
    },
  };

  downloadFile(extension: string, res: Blob): void {
    const now = new Date();
    let anchor = document.createElement('a');
    anchor.download = "WO_ " + now.toLocaleString('es-CL') + "." + extension;
    anchor.href = window.URL.createObjectURL(res);
    anchor.dataset.downloadurl = ['text/plain', anchor.download, anchor.href].join(':');
    anchor.click();
  }

  ngAfterViewInit(): void {
    this.renderer.listen(this.tableListOt.nativeElement, 'click', (event) => {
      // event.stopPropagation();
      if (event.target.hasAttribute("view-wo-id")) {
        let barcode = event.target.getAttribute("view-wo-id");
        this.router.navigate(["./tracking"], { relativeTo: this.activatedRoute, queryParams: { barcode: barcode} });
      }
      if (event.target.hasAttribute("view-wo-detail")) {
        // console.log(event.target.getAttribute("view-wo-detail"))
        // // this.router.navigate(["solicitudot/"], event.target.getAttribute("view-wo-detail"));
        this.router.navigate(["../solicitudot/" + event.target.getAttribute("view-wo-detail")],{ relativeTo : this.activatedRoute });

      }
    });
  }

  ngOnInit() {
  }

}
