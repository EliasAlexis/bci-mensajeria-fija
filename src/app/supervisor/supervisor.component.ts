import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-supervisor',
  template: '<app-full-layout></app-full-layout>',
  styleUrls: []
})
export class SupervisorComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
