import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable, EMPTY } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { LayoutService } from '../../layouts/services/layout.service';

@Injectable()
export class WOResolver implements Resolve<any> {
    constructor(
        private layoutService: LayoutService,
        private router: Router,
    ) { }

    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<any> | Promise<any> | any {
        if (route.params['id'] === 'new'){
            return 'new';
        }
        return this.layoutService.wiewWo(route.params['id'])
            .pipe(
                catchError(err => {
                    this.router.navigate(['404']);
                    return EMPTY;
                })
            )
    }
}