export class Profile{
    id: string;
    username: string;
    first_name: string;
    last_name: string;
    rut: string;
    phone: string;
    email: string;
    // branch:  Array<branch>;
    branch: Branch = new Branch();
    profile_picture?: string;
} 

export class Branch{
    name: string;
    address: string;
  } 

  export class PassReset{
    old_password: string;
    password: string;
    confirm_password: string;
  }