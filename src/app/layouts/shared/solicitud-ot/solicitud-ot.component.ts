import { Component, OnInit, ViewChild, ElementRef, NgZone, OnDestroy } from '@angular/core';
import { FormBuilder, Validators, FormControl, FormGroup} from '@angular/forms';
import { rutValidator } from '../../../extra/validators';
import { MapsAPILoader } from '@agm/core';
import { WoService } from '../../services/wo-service.service'
import { LayoutService } from '../../services/layout.service'
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router'
import { ToastrService } from 'ngx-toastr';
import { takeUntil, map, filter } from 'rxjs/operators';
import { Subject, Observable } from 'rxjs';
import { MetaTitleService } from '../../../services/meta-title.service';

const DetailError ={
  "sender_full_name": "Nombre completo origen",
  "receiver_full_name": "Nombre completo destinatario",
  "receiver_address": "Dirección destinatario", 
  "receiver_comune": "Comuna destinatario",
  "receiver_region": "Región destinatario",
  "sender_email": "Correo origen",
  "receiver_email": "Correo destinatario",
  "height": "Alto",
  "width": "Ancho",
  "depth": "Largo",
  "weight": "Peso",
  "sender_extra_info": "comentario",
  }

@Component({
  selector: 'app-solicitud-ot',
  templateUrl: './solicitud-ot.component.html',
  styleUrls: ['./solicitud-ot.component.scss']
})


export class SolicitudOtComponent implements OnInit, OnDestroy {
  @ViewChild("addressSearch", {static: true})  public searchElementRef: ElementRef;
  @ViewChild("addressSearch2", {static: true})  public searchElementRef2: ElementRef;
  
  id:number;
  codBarra = "";
  formDisabled = false;
  isUpdate = false;
  isElevated = false;
  selectedCompany:number;
  extra_fieldsJson = []

  private destroy$: Subject<boolean> = new Subject<boolean>();
  public dataAutoComplete = {}
  public contacts = [];
  public listService = [];
  public listaProducts = [];
  // public listaTramos: Observable<any>;
  public listaTramos:any;
  public countries = [];
  public companies:Observable<any>;

  formOT = this.formBuilder.group({
    sender_full_name : ['',Validators.required],
    sender_rut: ['', rutValidator()],
    sender_email: ['',[Validators.required,Validators.email]],
    sender_phone: ['',Validators.required],
    sender_address: ['',Validators.required],
    sender_comune: ['',Validators.required],
    sender_region: ['',Validators.required],
    sender_lat: ['', Validators.required],
    sender_lng: ['', Validators.required],
    sender_address_number: [''],
    sender_extra_info: [''],
    receiver_full_name: ['',Validators.required],
    receiver_rut: ['', rutValidator()],
    receiver_email: ['',[Validators.required,Validators.email]],
    receiver_phone: ['',Validators.required],
    receiver_address: ['',Validators.required],
    receiver_comune: ['',Validators.required],
    receiver_region: ['',Validators.required],
    receiver_lat: ['', Validators.required],
    receiver_lng: ['', Validators.required],
    receiver_address_number: [''],
    deliver_info: [''],
    has_return: ['no_return',Validators.required],
    height: ['0',Validators.required],
    width: ['0',Validators.required],
    depth: ['0',Validators.required],
    weight: ['0', Validators.required],
    product: ['1',Validators.required],
    service: ['1',Validators.required],
    segment: ['1'],
    notification_type: ['none',Validators.required],
    product_description: [''],
    extra_fields : this.formBuilder.group([]),
})

  constructor(
    private formBuilder: FormBuilder,
    private mapsAPILoader : MapsAPILoader,
    private ngZone : NgZone,
    private woService: WoService,
    private activatedRoute : ActivatedRoute,
    private toastrService : ToastrService,
    private router: Router,
    private layoutService: LayoutService,
    private metaTitleService: MetaTitleService,
  ) {
    this.metaTitleService.updateTitle();
    this.activatedRoute.data
    .pipe(
      takeUntil(this.destroy$)
    )
    .subscribe(
      (data:any) => {
        if (data.group === 'b3BlcmF0b3Jz' && data.wo != 'new'){
          this.isUpdate = true;
        }
        if (data.wo != 'new'){
          this.codBarra = data.wo.barcode
          this.getExtraFields(
            {company:data.wo.owner.company},
            data.wo.extra_fields
            )
          this.formOT.patchValue(data.wo);
          console.log(this.formOT.value)
          if (data.group === 'dXNlcnM='){
            this.formDisabled = true;
          }
        }else{
          if (data.group === 'b3BlcmF0b3Jz'){
            this.isElevated = true;
          }
          this.getExtraFields()
          // same component reset workarround
          this.formOT.reset({
            has_return: 'no_return',
            height: '0',
            width: '0',
            depth: '0',
            weight: '0',
            product: '1',
            service: '1',
            segment: '1',
            notification_type: 'none'
          })
          this.id = null;
          this.codBarra = "";
          this.formDisabled = false;
          this.isUpdate = false;
          this.router.events.subscribe(
            evt => {
              if (evt instanceof NavigationEnd){
                window.scroll(0, 0);
              }
            }
          )
        }
      }
    )
  }

  ngOnInit() {
    this.id = Number(this.activatedRoute.snapshot.paramMap.get('id'));
    
    this.getListService();
    this.listProducts();

    // this.listaTramos = this.woService.listSegments()
    //   .pipe(
    //     map(
    //       (res: any) => {
    //         return res.results.map( el => {
    //           el.prom = (parseFloat(el.min_weight) + parseFloat(el.max_weight))/2;
    //           return el
    //         });
    //       }
    //     )
    //   ) 
    
    this.woService.listSegments().subscribe((res:any)=>{
      this.listaTramos = res.results
      console.log(this.listaTramos)
    })
      

    this.companies = this.layoutService.getCompanies({limit: 999})
      .pipe(
        map(
          (res: any) => {
            return res.results;
          }
        )
      )
    
    this.formOT.controls["sender_full_name"].valueChanges
      .pipe(
        filter( val => val instanceof Object),
        takeUntil(this.destroy$)
      )
      .subscribe(
        (val:any) => {
          this.formOT.controls["sender_full_name"].setValue(val.full_name)
        }
      )
    this.formOT.controls["receiver_full_name"].valueChanges
      .pipe(
        filter( val => val instanceof Object),
        takeUntil(this.destroy$)
      )
      .subscribe(
        (val:any) => {
          this.formOT.controls["receiver_full_name"].setValue(val.full_name)
        }
      )
    this.mapsAPILoader.load().then( ()=> {
      let autocomplete = new google.maps.places.Autocomplete(
        this.searchElementRef.nativeElement, 
          { 
            types: ["address"],
            componentRestrictions: { country: "cl" }
          }
        );
      autocomplete.addListener("place_changed", ()=> {
        this.ngZone.run( ()=> {
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();
            if (place.geometry === undefined || place.geometry === null) {
              return;
            }
          let components = place.address_components;
          let addressValues = {
            // sender_address_number: components.find(el => el.types.includes("street_number")).long_name,
            sender_address: place.name,
            sender_region: components.find(el => el.types.includes("administrative_area_level_1")).long_name,
            sender_comune: components.find(el => el.types.includes("administrative_area_level_3")).long_name,
            sender_lat: Math.round(place.geometry.location.lat() * 10000000) / 10000000,
            sender_lng: Math.round(place.geometry.location.lng() * 10000000) / 10000000,
          }
          this.formOT.patchValue(addressValues);
        })
      });
    })
  
    // Direccion Destino 
    this.mapsAPILoader.load().then( ()=> {
      let autocomplete = new google.maps.places.Autocomplete(
        this.searchElementRef2.nativeElement, 
          { 
            types: ["address"],
            componentRestrictions: { country: "cl" }
          }
        );
      autocomplete.addListener("place_changed", ()=> {
        this.ngZone.run( ()=> {
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();
            if (place.geometry === undefined || place.geometry === null) {
              return;
            }
          let components = place.address_components;
          let addressValues = {
            receiver_address: place.name,
            receiver_region: components.find(el => el.types.includes("administrative_area_level_1")).long_name,
            receiver_comune: components.find(el => el.types.includes("administrative_area_level_3")).long_name,
            receiver_lat: Math.round(place.geometry.location.lat() * 10000000) / 10000000,
            receiver_lng: Math.round(place.geometry.location.lng() * 10000000) / 10000000,
          }
          this.formOT.patchValue(addressValues);
        })
      });
    })
  }

  get extra_fields(){
    return this.formOT.get('extra_fields') as FormGroup
  }

  getExtraFields(params?:any, extra?:any){
    this.woService.extra_fields(params)
    .pipe(
      takeUntil(this.destroy$)
    )
    .subscribe
      ((res: any) => {
        this.extra_fieldsJson = res;
        let field = {}
        for (let extra_field of res) {
          this.extra_fields.addControl(extra_field.name, new FormControl(''))
          field[extra_field.name] = ['']
        }
        if (extra){
          this.formOT.patchValue({extra_fields: extra})
        }
      })
  }

  getListService(){
    return this.woService.listService()
    .subscribe((response :any)=>{
     this.listService = response.results
    })
  }

  SaveOt(){
    let data = {...this.formOT.value};
    for (let el in data) {
      if (data[el] == null) {
        data[el] = "";
      }
    }
    for (let el in data.extra_fields){
      if (data.extra_fields[el] == null) {
        data.extra_fields[el] = "";
      }
    }
    if (this.isElevated){
      data.company = this.selectedCompany;
      if(!data.company){
        this.toastrService.warning("Debe seleccionar empresa");
        return
      }
    }
    this.formDisabled = true
    if (this.isUpdate){
      this.woService.replaceOt(this.id, data)
      .pipe(
        takeUntil(this.destroy$)
      )
      .subscribe(
        (response : any)=>{
          this.formDisabled = false
          this.toastrService.success('Datos guardados correctamente','', { timeOut: 6000 })
          this.router.navigate(['../../dashboard'], { relativeTo: this.activatedRoute})
        },error=>{
          this.formDisabled = false
          if (error.status === 500){
            this.toastrService.error("Ha ocurrido un problema");
          }
          else{
            for (let errorHead in error.errMsg){
              this.toastrService.error(error.errMsg[errorHead],DetailError[errorHead]);    
            }    
          }
        }
        )
      }else{
      this.woService.saveOtService(data)
        .pipe(
          takeUntil(this.destroy$)
        )
        .subscribe((response :any)=>{
          this.formDisabled = false
          this.toastrService.success('Datos guardados correctamente',' N° de solicitud :  '+ response.barcode, { timeOut: 6000 }) 
          this.formOT.reset({
            has_return: 'no_return',
            height: '0',
            width: '0',
            depth: '0',
            weight: '0',
            product: '1',
            service: '1',
            segment: '1',
            notification_type: 'none'
          })  
        },error=>{
          // console.log(error);
          this.formDisabled = false
          if (error.status === 500){
            this.toastrService.error("Ha ocurrido un problema");
          } else{
            for (let errorHead in error.errMsg){
              this.toastrService.error(error.errMsg[errorHead],DetailError[errorHead]); 
              }
            }
        }
      )
    }
  }
  
  listContacFilter(){
    this.woService.listContacService()
    .pipe(
      takeUntil(this.destroy$)
    )
    .subscribe((response : any)=>{
      this.countries = response.results
    })
  }

  listProducts(){
    this.woService.listProductsService()
    .pipe(
      takeUntil(this.destroy$)
    )
    .subscribe((res : any)=>{
      this.listaProducts = res.results;
    })
  }

  selectEvent(item) {
    let dataConact = {
    sender_full_name : item.full_name,
    sender_rut: item.rut,
    sender_email :item.email,
    sender_phone  :item.phone,
    sender_address:item.address,
    sender_comune : item.comune,
    sender_region :item.region,
    sender_address_number : item.address_number,
    sender_lat: item.lat,
    sender_lng: item.lng
    }
    this.formOT.patchValue(dataConact);
  }

  updateExtraFields(){
    this.formOT.controls['extra_fields'] = this.formBuilder.group([]);
    this.getExtraFields(
      {company: this.selectedCompany}
    )
  }
  
  onChangeSearch(val: string) {
    // this.formOT.patchValue({sender_full_name: val});
    this.woService.listContacService({ full_name__icontains: val})
    .pipe(
      takeUntil(this.destroy$)
    )
    .subscribe(
      (res : any) => {
        this.contacts = res.results;
      }
    )
  }

  selectEventReceiver(item) {
    let dataConact = {
      receiver_full_name : item.full_name,
      receiver_rut: item.rut,
      receiver_email :item.email,
      receiver_phone  :item.phone,
      receiver_address:item.address,
      receiver_comune : item.comune,
      receiver_region :item.region,
      receiver_address_number : item.address_number,
      receiver_lat: item.lat,
      receiver_lng: item.lng
      }
    
    this.formOT.patchValue(dataConact);
  }
  
  onChangeSearchReceiver(val: string) {
    // this.formOT.patchValue({receiver_full_name: val});
    this.woService.listContacService({ full_name__icontains: val})
    .pipe(
      takeUntil(this.destroy$)
    )
    .subscribe(
      (res : any) => {
        this.contacts = res.results;
      }
    )
  }

  ngOnDestroy(){
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }
}
