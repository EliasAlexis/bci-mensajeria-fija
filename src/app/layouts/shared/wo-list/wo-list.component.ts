import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { DatePipe } from '@angular/common';
import { FormBuilder } from '@angular/forms';
import { DownloadService } from '../../../services/download.service';
import { LayoutService } from '../../services/layout.service';
import { takeUntil, map, switchMap, distinctUntilChanged, debounceTime, mergeMap} from 'rxjs/operators';
import { Subject, Observable, of, BehaviorSubject } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MetaTitleService } from '../../../services/meta-title.service';

class DateOnlyPipe extends DatePipe {
  public transform(value): any {
    return super.transform(value, 'dd-MM-yyyy HH:mm');
  }
}

@Component({
  selector: 'app-wo-list',
  templateUrl: './wo-list.component.html',
  styleUrls: ['./wo-list.component.scss']
})
export class WoListComponent implements OnInit, OnDestroy {
  @ViewChild('filterModal', { static: true }) filtersModal: ElementRef;
  @ViewChild('detailButton', { static: true }) detailButton: ElementRef;
  @ViewChild('trackingButton', { static: true }) trackingButton: ElementRef;

  transporters: Observable<any>;
  private currentCompany: number;
  searchTerm: BehaviorSubject<string> = new BehaviorSubject<string>('');
  filterTerm: Subject<string> = new Subject<string>();
  private destroy$: Subject<boolean> = new Subject<boolean>();
  companies: Observable<any>;
  branches: Observable<any>;
  users: Observable<any>;
  columnaEditar = true;

  currentPage: any = {};
  currentRows: any = [];
  tableColums: any = []

  constructor(
    private layoutService: LayoutService,
    private downloadService: DownloadService,
    private fb: FormBuilder,
    private modalService: NgbModal,
    private metaTitleService: MetaTitleService,
    ) {
      this.metaTitleService.updateTitle();
    }
    
  filters = this.fb.group({
    company: [''],
    branch: [''],
    user: [''],
    created_at__gte: [''],
    created_at__lte: [''],
    transporter: [''],
    status: [''],
    search: [''],
    ordering: [''],
    offset: [0],
    limit: [10],
  })

  setPage(pageInfo, store?:boolean) {
    this.filters.patchValue({ offset: pageInfo.offset * this.filters.value.limit})
    if(store){
      localStorage.setItem(
        'wo_filter_perst',
        JSON.stringify(this.filters.value)
      )
    }
    this.layoutService.listWO(this.filters.value)
      .pipe(
        takeUntil(this.destroy$)
      )
      .subscribe(
        (response: any) => {
          this.currentRows = response.results;
          this.currentPage = {
            count: response.count,
            limit: this.filters.value.limit,
            offset: pageInfo.offset,
          }
        },
        error => {
          console.log(error);
        }
      )
  }

  downloadExcel(){
    let params = {
      report_type: 'excel',
      ...this.filters.value
    }
    this.downloadService.downloadReport('wo/reports/', params)
      .subscribe(
        res => {
          this.downloadFile('xlsx', res)
        }
      )
  }

  downloadPdf(){
    let params = {
      report_type: 'pdf',
      ...this.filters.value
    }
    this.downloadService.downloadReport('wo/reports/', params)
      .subscribe(
        res => {
          this.downloadFile('pdf', res)
        }
      )
  }

  ngOnInit() {

    let dividir = window.location.pathname.split('/')
    for (let div of dividir) {
      if(div == 'trader')
        this.columnaEditar = false 
      }
    // console.log(this.columnaEditar)

    this.companies = this.layoutService.getCompanies({limit: 999})
      .pipe(
        map(
          (res: any) => {
            return res.results;
          }
        )
      )

    this.branches = this.filters.controls['company'].valueChanges.pipe(
      switchMap(
        val => {
          if (val == null){
            this.filters.patchValue({ company: '' })
            return of([])
          }else{
            return this.layoutService.getBranches(val, {limit: 999})
              .pipe(
                map( (val: any) => val.results)
              )
          }
        }
      )
    )

    this.users = this.filters.controls['branch'].valueChanges.pipe(
      switchMap(
        val => {
          if (val == null) {
            this.filters.patchValue({ branch: '' })
            return of([])
          } else {
            return this.layoutService.getUsers(this.filters.value.company , { branch: val, limit: 999 })
              .pipe(
                map((val: any) => val.results)
              )
          }
        }
      )
    )
    this.layoutService.retieveUserInfo()
      .pipe(
        takeUntil(this.destroy$)
      )
      .subscribe(
        user => {
          this.currentCompany = user.branch.company_id;
        }
      )
    this.transporters = this.searchTerm
      .pipe(
        distinctUntilChanged(),
        debounceTime(400),
        mergeMap(
          term => {
            return this.layoutService.getUsers(this.currentCompany, { groups: 6, ordering: 'first_name', search: term })
              .pipe(
                map(
                  (res: any) => {
                    return res.results;
                  }
                )
              )
          }
        )
      )
    this.filters.controls['user'].valueChanges.pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      val => {
        if (val == null) {
          this.filters.patchValue({ user: '' })
        }
      }
    )
    this.filters.controls['transporter'].valueChanges.pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      val => {
        if (val == null) {
          this.filters.patchValue({ transporter: '' })
        }
      }
    )
    this.tableColums = [
      {
        name: 'N° OT',
        prop: 'barcode',
        width: '70',
      },
      {
        name: 'Fecha emisión',
        prop: 'created_at',
      },
      {
        name: 'Empresa',
        prop: 'company',
        sort_prop: 'owner__branch__company__commercial_business'
      },
      {
        name: 'Destinatario',
        prop: 'receiver_full_name'
      },
      {
        name: 'Dirección destino',
        prop: 'receiver_address'
      },
      {
        name: 'Comuna destino',
        prop: 'receiver_comune'
      },
      {
        name: 'Último estado',
        prop: 'current_step.step_detail',
        sort_prop: 'current_step__status__name'
      },
      {
        name: 'Fecha último estado',
        prop: 'last_log.created_at',
        pipe: new DateOnlyPipe('en'),
        sort_prop: 'work_order_logs__created_at'
      },
      {
        name: 'Detalle',
        prop: 'id',
        orderable: false,
        cellTemplate: this.detailButton,
        cellClass: 'text-center',
        width: '90',
        sortable: false,
      },
      {
        name: 'Seguimiento',
        prop: 'barcode',
        cellTemplate: this.trackingButton,
        cellClass: 'text-center',
        width: '90',
        sortable: false,
      }
    ]
    if (!this.columnaEditar) {
      let index = this.tableColums.findIndex(el => {
        return el.name == 'Detalle';
      })
      this.tableColums.splice(index, 1);
    }
   
    this.filterTerm
    .pipe(
      distinctUntilChanged(),
      debounceTime(300),
      takeUntil(this.destroy$)
      )
      .subscribe(
        (val: any) => {
          this.filters.patchValue({ search: val})
          this.setPage({offset: 0})
        }
        )
    let storedFilters: any;
    try{
      storedFilters = JSON.parse(localStorage.getItem('wo_filter_perst'));
    } catch (SyntaxError){
      storedFilters = {};
    }
    if (!storedFilters){
      storedFilters = {};
    }
    this.filters.patchValue(storedFilters)
    console.log((storedFilters.offset || 0))
    console.log((storedFilters.limit || 1))
    this.setPage({ offset: (storedFilters.offset || 0) / (storedFilters.limit || 1) });
  }

  downloadFile(extension: string, res: Blob): void {
    const now = new Date();
    let anchor = document.createElement('a');
    anchor.download = 'WO ' + now.toLocaleString('es-CL') + '.' + extension;
    anchor.href = window.URL.createObjectURL(res);
    anchor.dataset.downloadurl = ['text/plain', anchor.download, anchor.href].join(':');
    anchor.click();
    document.removeChild(anchor);
  }

  open(content) {
    this.modalService.open(content)
  }

  doFilter(reset:boolean){
    if(reset == true){
      this.filters.reset({
        company: '',
        branch: '',
        user: '',
        created_at__gte: '',
        created_at__lte: '',
        status: '',
        search: '',
        ordering: '',
        limit: 10,
      })
      localStorage.removeItem('wo_filter_perst')
    }
    this.setPage({offset: 0}, true)
    this.modalService.dismissAll();
  }

  doSort(event){
    console.log(event)
    this.filters.patchValue({ 
      ordering: (event.newValue == 'asc' ? '' : '-') + (event.column.sort_prop == undefined ? event.column.prop : event.column.sort_prop)
    })
    this.setPage({ offset: 0 }, true);
  }

  search(search: string, item: any): boolean {
    return true;
  }

  fila(num){
    console.log(num);
    this.filters.patchValue({limit:num})
    this.setPage({ offset: 0 }, true);
  }

  ngOnDestroy(){
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }
}
