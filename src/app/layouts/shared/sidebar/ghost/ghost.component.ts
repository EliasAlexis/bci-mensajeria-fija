import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'sidebar-ghost',
  template: `
    <div  *ngFor='let it of ghosts' 
        class='list-elem d-flex align-items-center' >
      <div class="icon"></div>
      <div class="lines d-flex align-items-center">
        <h3 class="m-0"></h3>
      </div>
    </div> 
`,
  styleUrls: ['./ghost.component.scss']
})
export class GhostComponent implements OnInit {
  @Input() ghosts : any[];
  constructor() { }

  ngOnInit() {
  }

}
