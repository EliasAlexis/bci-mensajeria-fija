import { Component, OnInit, Input } from '@angular/core';
//import { ROUTES } from './menu-items';
import { RouteInfo } from './sidebar.metadata';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs';
import { trigger } from '@angular/animations';

import { fadeIn } from '../../animations/fade-animations';
declare var $: any;

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  animations: [ trigger('fadeIn', fadeIn(':enter')) ],
})
export class SidebarComponent implements OnInit {
  @Input() sidebarnavItems : any[];
  //sidebarnavItems = [];
  showMenu = '';
  showSubMenu = '';
  // this is for the open close
  addExpandClass(element: any, length: number) {
    console.log(element)
    if (element === this.showSubMenu){
      this.showSubMenu = '0';
    }else{
      if(length > 0){
        this.showSubMenu = element;
      }
    }
    if (element === this.showMenu) {
      this.showMenu = '0';
    } else {
      this.showMenu = element;
    }
    
  }

  constructor(
    private modalService: NgbModal,
    private router: Router,
    private route: ActivatedRoute,
  ) {}

  // End open close
  ngOnInit() { }
}
