import { Directive, ElementRef, NgZone} from '@angular/core';
import { MapsAPILoader } from '@agm/core';
import { ControlContainer } from "@angular/forms";

@Directive({
  selector: '[appFichaAdres]'
})
export class FichaAdresDirective {
  constructor(
    private elementRef : ElementRef,
    private mapsAPILoader : MapsAPILoader,
    private ngZone : NgZone,
    private ControlContainer: ControlContainer,
  ){
    this.mapsAPILoader.load().then(() => {
      let autocomplete = new google.maps.places.Autocomplete(
        this.elementRef.nativeElement,
        {
          types: ["address"],
          componentRestrictions: { country: "cl" }
        }
      );
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
          let components = place.address_components;
          let addressValues = {
            address: place.name,
            comune: components.find(el => el.types.includes("administrative_area_level_3")).long_name,
            province: components.find(el => el.types.includes("administrative_area_level_2")).long_name,
            region: components.find(el => el.types.includes("administrative_area_level_1")).long_name
          }
          let newValue = addressValues.address + ", " + addressValues.comune + ", " + addressValues.region
          const control = this.ControlContainer.control.get('address')
          control.patchValue(newValue)
        })
      });
    })
  }
}
