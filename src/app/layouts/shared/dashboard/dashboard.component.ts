import { Component, OnInit } from '@angular/core';
import { LayoutService } from "../../services/layout.service";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(
    private layoutService : LayoutService,
  ) { }

  ngOnInit() {
  }

  dtOptions: any = {
    dom: 'Bfrtip',
    serverSide: true,
    language: {
      url: '../../../assets/languages/Spanish.json'
    },
   processing: true,
     buttons: {
      buttons:[
           {
             text: 'Crear',
             key: '1',
             className: 'btn btn-primary btn-sm',
             action: (e, dt, node, config) => {
              // this.router.navigate(['./create-user'],{relativeTo : this.route});
             }
           },
         {
           text: 'Excel',
           key: '2',
           className: 'btn btn-success btn-sm text-white',
           action: (e, dt, node, config) => {
           alert('Button activated');
           }
         },
       ]
    },
    columns: [
      { 
        title: 'Usuario',
        data: 'full_name'
      },{ 
        title: 'rut',
        data: 'rut'
      },{ 
        title: 'email',
        data: 'email'
      },{ 
        title: 'phone',
        data: 'phone'
      },{ 
        title: 'address',
        data: 'address'
      }
      // ,{ 
      //   title: 'Editar',
      //   data: 'id',
      //   orderable: false,
      //   className: 'text-center',
      //   render: function (data: any, type: any, full: any) {
      //     return `<button type="button" class="btn btn-sm btn-info" idEditUser="${data}"><i class="fas fa-edit" idEditUser="${data}"></i>
      //           </button>`
      //   }
      // }
    ],
    ajax : (data : any, callback ) => {
      let order = data.order[0];
      let consulta: any = {
        search: data.search.value,
        offset: data.start,
        limit: data.length,
        ordering: (order.dir == 'desc' ? '-' : '') + data.columns[order.column].data
      };
      
      this.layoutService.addContacService()
      .subscribe(
        (response: any) => {
        // console.log("ok");
        // console.log(response);
        callback({
        recordsTotal: response.count,
        recordsFiltered: response.count,
        data: response.results
        });
        },
        error => {
        // console.log("error");
        // console.log(error);
        }
      )
    },  
};

}
