// Modules
import { NgModule } from '@angular/core';
import { CommonModule,CurrencyPipe } from '@angular/common';
import { FullComponent } from './containers/full/full.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { RouterModule } from '@angular/router';
import { DataTablesModule } from 'angular-datatables';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxSelectModule } from 'ngx-select-ex';
import {AutocompleteLibModule} from 'angular-ng-autocomplete';
import { CurrencyMaskModule } from "ng2-currency-mask";
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

// Components
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { NavigationComponent } from './shared/navigation/navigation.component';
import { SidebarComponent } from './shared/sidebar/sidebar.component';
import { GhostComponent } from './shared/sidebar/ghost/ghost.component';
import { FichaComponent } from './shared/ficha/ficha.component';
import { WoListComponent } from './shared/wo-list/wo-list.component'
import { SolicitudOtComponent} from "./shared/solicitud-ot/solicitud-ot.component";
import { BulkComponent } from './shared/solicitud-ot/bulk/bulk.component';
import { DashboardComponent } from './shared/dashboard/dashboard.component';

// Directives
import { ChileanMaskDirective, DisableControlDirective } from '../extra/directives';

// Etc
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';

import { LayoutService } from './services/layout.service';
// import { E404Component } from '../commons/e404/e404.component';
import { FichaAdresDirective } from './shared/directive/ficha-adres.directive';
import { NgxMaskModule, IConfig } from 'ngx-mask';
import { FormatMoneyDirective } from './shared/directive/format-money.directive';
import { FormatMoneyPipe } from './shared/pipe/format-money.pipe'
import { WoService } from './services/wo-service.service';
// import { ImportdatafallaComponent } from './shared/sidebar/importdatafalla/importdatafalla.component';

export const options: Partial<IConfig> | (() => Partial<IConfig>) = {}

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true,
  wheelSpeed: 1,
  wheelPropagation: true,
  minScrollbarLength: 20
};

const COMPONENTS = [
  FullComponent,
  NavigationComponent,
  SidebarComponent,
  GhostComponent,
  ChileanMaskDirective,
  FichaComponent,
  DisableControlDirective,
  WoListComponent,
  SolicitudOtComponent,
]

@NgModule({
  declarations: [...COMPONENTS, FichaAdresDirective, DashboardComponent, FormatMoneyDirective, FormatMoneyPipe, BulkComponent,  

  ],
  imports: [
    CurrencyMaskModule,
    CommonModule,
    FontAwesomeModule,
    PerfectScrollbarModule,
    RouterModule,
    DataTablesModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    NgxMaskModule,
    NgxSelectModule,
    AutocompleteLibModule,
    NgxDatatableModule,
    ],
  providers: [
	  {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG,
    },
    LayoutService,
    CurrencyPipe,
    WoService,
  ],
  exports: [
    ...COMPONENTS,
    FontAwesomeModule,
    DataTablesModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    FichaAdresDirective,
    NgxSelectModule,
    CurrencyMaskModule,
  ],
})
export class LayoutsModule { }
