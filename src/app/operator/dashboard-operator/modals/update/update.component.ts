import { Component, OnInit, ViewChild, OnDestroy, SecurityContext } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser'
import { ServiceOperatorService } from "../../../service/service-operator.service";
import { ToastrService } from "ngx-toastr";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { takeUntil, finalize } from 'rxjs/operators';

const headStatus = {
  0: "Anulado",
  2: "Asignación retiro",
  4: "Central",
  5: "Asignación entrega",
  6: "Exitoso",
  7: "Fallido",
  9: "Cerrado",
  "detail": "Codbarra",
  "code": "Error"
}
@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.scss']
})
export class UpdateComponent implements OnInit, OnDestroy {
  @ViewChild(DataTableDirective, { static: true }) asignTable: DataTableDirective;

  formRebaja = this.formBuilder.group({
    code: ['', Validators.required],
    deliver_info: [''],
    barcode: ['', Validators.required],
  })
  dtTrigger: Subject<boolean> = new Subject<boolean>();
  destroy$: Subject<boolean> = new Subject<boolean>();
  dataTableRebaja = [];
  dtOptionsDataTableRebaja: any = {
    searching: false,
    data: this.dataTableRebaja,
    language: {
      url: '../../../assets/languages/Spanish.json'
    },
    columns: [
      {
        title: 'Estado',
        data: 'code'
      },
      {
        title: 'Obs',
        className: 'obs-cell',
        data: 'deliver_info'
      },
      {
        title: 'Cod barra',
        data: 'barcode'
      },
      {
        title: '',
        data: 'status',
        render: function (data: any, type: any, full: any) {
          switch (data) {
            case "Cargando":
              return `<label class="text-warning">${data}</label>`;
              break;
            case "Ok":
              return `<label class="text-success">${data}</label>`
              break;
            default:
              return `<label class="text-danger">${data}</label>`;
          }
        }
      }
    ]
  };

  constructor(
    private formBuilder: FormBuilder,
    private serviceOperatorService: ServiceOperatorService,
    private toastrService: ToastrService,
    public activeModal: NgbActiveModal,
    private domSanitizer: DomSanitizer,
  ) { }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  private doReload() {
    this.asignTable.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.clear().draw();
      dtInstance.rows.add(this.dataTableRebaja)
      dtInstance.columns.adjust().draw();
    })
  }

  saveRebaja() {
    if (this.formRebaja.valid) {
      let jsonConcatRebaja = {
        "code": headStatus[this.formRebaja.value.code],
        "deliver_info": this.formRebaja.value.deliver_info?this.domSanitizer.sanitize(
                          SecurityContext.HTML,
                          this.formRebaja.value.deliver_info
                          ).slice(0, 32) + '...':'',
        "barcode": this.domSanitizer.sanitize(SecurityContext.HTML, this.formRebaja.value.barcode),
        "status": "Cargando",
      }
      this.dataTableRebaja.push(jsonConcatRebaja);
      let current_data = { ...this.formRebaja.value }
      this.doReload();
      this.formRebaja.controls['barcode'].reset();
      this.formRebaja.controls['deliver_info'].reset();
      current_data.barcode = current_data.barcode.toUpperCase();
      this.serviceOperatorService.saveDashboardMdalServ(current_data)
        .pipe(
          finalize(() => {
            this.doReload();
          }),
          takeUntil(this.destroy$),
        )
        .subscribe((response: any) => {
          jsonConcatRebaja.status = "Ok"
        },
        error => {
          if (error.error && error.status != 500) {
            if (error.error.detail) {
              jsonConcatRebaja.status = error.error.detail
            } else if (error.error.code) {
              jsonConcatRebaja.status = error.error.code[0];
            } else {
              jsonConcatRebaja.status = "Error"
              console.log(error.error);
            }
            // for (var errorHead in error.error) {
            //   this.toastrService.error(error.error[errorHead], headStatus[errorHead])
            // }
          } else {
            jsonConcatRebaja.status = "Error"
            console.log(error);
          }
        })
    } else {
      if (this.formRebaja.value.code == "" || this.formRebaja.value.code == null)
        this.toastrService.warning("Este campo requerido", "Estado")

      if (this.formRebaja.value.barcode == "" || this.formRebaja.value.barcode == null)
        this.toastrService.warning("Este campo es requerido", "Codigo de barra")
    }

  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
    this.dtTrigger.unsubscribe();
  }
}
