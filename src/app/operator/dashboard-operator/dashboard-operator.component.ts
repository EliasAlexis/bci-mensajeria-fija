import { Component, OnInit,ViewChild,ElementRef , Renderer2, OnDestroy} from '@angular/core';
import { ServiceOperatorService } from "../service/service-operator.service";
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from "ngx-toastr";
import { DataTableDirective,  } from 'angular-datatables';
import { AssignComponent } from './modals/assign/assign.component';
import { UpdateComponent } from './modals/update/update.component';
import { environment } from '../../../environments/environment';
import { retryWhen, delay } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { FormBuilder } from '@angular/forms';
import { takeUntil, distinctUntilChanged, debounceTime} from 'rxjs/operators';
import { MetaTitleService } from '../../services/meta-title.service';
import { filter } from 'underscore';

@Component({
  selector: 'app-dashboard-operator',
  templateUrl: './dashboard-operator.component.html',
  styleUrls: ['./dashboard-operator.component.scss']
})
export class DashboardOperatorComponent implements OnInit, OnDestroy {

  @ViewChild('printWO', { static: true }) printWO : ElementRef;
  @ViewChild(DataTableDirective,{ static: true })   dtElement: DataTableDirective;
  @ViewChild('columsOrigen', { static: true }) columsOrigen: ElementRef;
  @ViewChild('columsDestino', { static: true }) columsDestino: ElementRef;
  @ViewChild('columsDia', { static: true }) columsDia: ElementRef;
  @ViewChild('columsEstado', { static: true }) columsEstado: ElementRef;
  @ViewChild('columsFUE', { static: true }) columsFUE: ElementRef;
  @ViewChild('selectOt', { static: true }) selectOt: ElementRef;
  @ViewChild('btns', { static: true }) btns: ElementRef;
 
  // webSocket = new WebSocket('ws://api.postal.digitechqa.cl/wo_notifier/?token='+localStorage.getItem('jtoken'));
 
  destroy$ = new Subject<boolean>();
  webSocketData: Array<any> = new Array<any>();
  overallCount: number;
  currentButtons: Array<any> = new Array<number>();
  buttonsData: Array<any> = new Array<any>();
  printQueue: Array<string> = [];
  dtOptions: any = {};
  filteringArgs: any;
  currentTablePage: any[];
  filterTerm: Subject<string> = new Subject<string>();
  private
  today = new Date().toLocaleDateString();
  columnaEditar = true;
  dashSock: any;

  currentPage: any = {};
  currentRows: any = [];
  tableColums: any = []

  private excluded_status = [0, 6, 9]

  private printModalInstance: NgbModalRef;

  constructor(
    private serviceOperatorService : ServiceOperatorService,
    private modalService: NgbModal,
    private toastrService : ToastrService, 
    private renderer : Renderer2,
    private fb: FormBuilder,
    private metaTitleService: MetaTitleService,
    
  ) {
    this.metaTitleService.updateTitle();
    // this.dtOptions = {
    //   dom: 'frtip',
    //   serverSide: true,
    //   processing: true,
    //   searchDelay: 350,
    //   order: [[0, "desc"]],
    //   language: {
    //     url: '../../../assets/languages/Spanish.json'
    //   },
    //   buttons: {
    //     buttons: [
    //       {
    //         text: 'Excel',
    //         key: '2',
    //         className: 'btn btn-success btn-sm text-white',
    //         action: (e, dt, node, config) => {
    //           alert('Button activated');
    //         }
    //       },
    //     ]
    //   },
    //   columns: [
    //     {
    //       data: null,
    //       width: '13%'
    //     },
    //     {
    //       data: 'company',
    //       width: '12%'
    //     },
    //     {
    //       data: 'sender_address',
    //       width: '22%'
    //     },
    //     {
    //       data: 'receiver_address',
    //       width: '22%'
    //     },
    //     {
    //       data: 'created_at',
    //       width: '10%'
    //     },
    //     {
    //       data: 'date',
    //       width: '10%',
    //       render: function (data: any, type: any, full: any) {
    //         return new Date();
    //       }
    //     },
    //     {
    //       data: 'current_step.name',
    //       width: '10%'
    //     },{
    //       data: 'transporter',
    //       width: '10%'
    //     },{
    //       data: null,
    //       orderable: false,
    //       width: '10%'
    //     },
    //   ],
    //   ajax: (data: any, callback) => {
    //     let order = data.order[0];
    //     let consulta: any = {
    //       search: data.search.value,
    //       offset: data.start,
    //       limit: data.length,
    //       ordering: (order.dir == 'desc' ? '-' : '') + data.columns[order.column].data,
    //       excluded_status__in: this.excluded_status.toString(),
    //       ...this.filteringArgs,
    //     };
    //     this.serviceOperatorService.listOtService(consulta)
    //       .pipe(
    //         takeUntil(this.destroy$)
    //       )
    //       .subscribe(
    //         (response: any) => {
    //           this.currentTablePage = response.results;
    //           console.log(this.currentTablePage)
    //           callback({
    //             recordsTotal: response.count,
    //             recordsFiltered: response.count,
    //             data: []
    //           });
    //         },
    //         error => {
    //           console.log(error);
    //           // if (error.error && error.status != 500) {
    //           //   for (var errorHead in error.error) {
    //           //     this.toastrService.error(error.error[errorHead])
    //           //   }
    //           // }
    //         }
    //       )
    //   },
    // };
  
  }

  filters = this.fb.group({
    company: [''],
    search: [''],
    ordering: [''],
    offset: [0],
    limit: [10],
    excluded_status__in: this.excluded_status.toString(),
    excluded_companies__in: this.currentButtons.toString()
  })

  setPage(pageInfo, store?:boolean) {
    this.filters.patchValue({ offset: pageInfo.offset * this.filters.value.limit})
    this.serviceOperatorService.listOtService(this.filters.value)
      .pipe(
        takeUntil(this.destroy$)
      )
      .subscribe(
        (response: any) => {
          this.currentRows = response.results;
          let cont = 0
          for(let selectedOt of this.currentRows ){ 
            this.currentRows[cont].selected = this.printQueue.includes(selectedOt.barcode)
            cont  = cont + 1
          }
          // console.log(this.currentRows)

          this.currentPage = {
            count: response.count,
            limit: this.filters.value.limit,
            offset: pageInfo.offset,
          }
        },
        error => {
          console.log(error);
        }
      )
  }
  
  search(search: string, item: any): boolean {
    return true;
  }

  
  ngOnInit() {

    this.tableColums = [
      {
        name: '',
        prop: 'barcode',
        cellTemplate: this.btns,
        sortable: false,
        width: '100'
      },
      {
        name: 'N°',
        prop: 'barcode',
        width: '70',
        cellTemplate: this.selectOt,
      },
      {
        name: 'Estado',
        prop: 'current_step.step_detail',
        cellTemplate: this.columsEstado,
        sort_prop: 'current_step__status__name',
      },
      {
        name: 'Empresa',
        prop: 'company',
        sort_prop: 'owner__branch__company__commercial_business',
      },
      {
        name: 'Origen',
        prop: 'sender_address',
        cellTemplate: this.columsOrigen,
        width: '300'
      },
      {
        name: 'Destino',
        prop: 'receiver_address',
        cellTemplate: this.columsDestino,
        width: '300'
      }, 
      {
        name: 'Fecha Creación',
        prop: 'created_at'
      },
      {
        name: 'Días',
        prop: 'created_at',
        cellTemplate: this.columsDia,
      },
      {
        name: 'Servicio',
        prop: 'service_name'
      }, 
      {
        name: 'Patente',
        prop: 'current_plate',
        sort_prop: 'work_order_logs__transport__plate',
      },
      {
        name: 'Fecha último estado',
        prop: 'last_log.created_at',
        sort_prop: 'work_order_logs__created_at',
        cellTemplate: this.columsFUE,
      },
      {
        name: 'Conductor',
        prop: 'transporter',
        sort_prop: 'work_order_logs__transporter__first_name',
      },
    ]

    this.setPage({ offset: 0 });
    this.filterTerm
      .pipe(
        distinctUntilChanged(),
        debounceTime(300),
        takeUntil(this.destroy$)
      )
      .subscribe(
        (val: any) => {
          this.filters.patchValue({ search: val})
          this.setPage({offset: 0})
        }
      )

    let dividir = window.location.pathname.split('/')
    for (let div of dividir) {
      if(div == 'trader')
        this.columnaEditar = false 
      }

    let buttons = localStorage.getItem("buttons");
    if(buttons){
      this.currentButtons = buttons.split(',');
      this.filteringArgs = {
        excluded_companies__in: this.currentButtons.toString()
      }
    }
    this.dashSock = this.serviceOperatorService.connectSocket('wo_notifier');
    this.fireSocket();
    this.dashSock
    .pipe(
      retryWhen(err => err.pipe(delay(5000))),
      takeUntil(this.destroy$)
    )
    .subscribe(
      event => {
        switch (event.type){
          case "operator.exclude_clients":
            this.webSocketData = [...event.data];
            this.overallCount = event.data
              .reduce((ac: number, curr: any) => {
                return ac + parseInt(curr.works_orders) 
              }, 0)
            break;
          case "operator.filter_clients":
            this.buttonsData = [...event.data];
            break;
          case "operator.fire_update":
            this.fireUpdates();
            break;
        }
      }
    )
  }
  
  doSort(event){
    console.log(event)
    this.filters.patchValue({ 
      ordering: (event.newValue == 'asc' ? '' : '-') + (event.column.sort_prop == undefined ? event.column.prop : event.column.sort_prop)
    })
    this.setPage({ offset: 0 }, true);
  }
  
  fechaFormato = function(f1){
    const DIAS = ["01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32"];
     f1 = new Date(f1)

    let dia = DIAS[f1.getDate()-1]
    let mes = DIAS[f1.getMonth()]
    let año = f1.getFullYear()
    let hrs = f1.getHours()
    let min = f1.getMinutes()
    // let seg = dif1.getSeconds()

    f1 = dia+"-"+mes+"-"+año+ " "+hrs+":"+min
    return f1;
    }

  restaFechas = function(f1,f2)
    {
    const DIAS = ["01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32"];

    let  fechahoy = new Date()

    let dia = DIAS[fechahoy.getDate()]
    let mes = DIAS[fechahoy.getMonth()]
    let año = fechahoy.getFullYear()

    f1 = dia+"/"+mes+"/"+año

    var f1 = f1.replace("/","-")
    f1 = f1.replace("/","-")
    f2 = f2.replace("/","-")
    f2 = f2.substring(0,10);

    var aFecha1 = f1.split('-');
    var aFecha2 = f2.split('-');
    var fFecha1 = Date.UTC(aFecha1[2],aFecha1[1]-1,aFecha1[0]);
    var fFecha2 = Date.UTC(aFecha2[2],aFecha2[1]-1,aFecha2[0]);
    var dif = fFecha1 - fFecha2;
    var dias = Math.floor(dif / (1000 * 60 * 60 * 24));
    return dias-1;
    }

  private fireSocket(){
    this.dashSock
      .next({
        type: "operator.exclude_clients",
        filters: {
          id__exclude: this.currentButtons.toString(),
          excluded_status__in: this.excluded_status.toString(),
        }
      })
    if (this.currentButtons.length > 0) {
      this.dashSock
        .next({
          type: "operator.filter_clients",
          filters: {
            id__in: this.currentButtons.toString(),
            excluded_status__in: this.excluded_status.toString(),
          }
        })
    }else{
      this.buttonsData = [];
    }
  }

  private fireUpdates(){
    this.fireSocket();
    // this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
    //   dtInstance.ajax.reload(() => { }, false);
    // });
    this.setPage({ offset: 0 }, true);
  }

  private findChildren(element: any){
    if (element.classList.contains("bottom-right")) {
      return element;
    }
    for (let el of element.children){
      if (el.classList.contains("bottom-right")){
        return el;
      } else if (el.children.length > 0){
        let child = this.findChildren(el);
        if (child) {
          return child;
        }
      }
    }
  }

  private findSibbling(el: any){
    return this.renderer.nextSibling(el);
  }

  addPrintQueque(barcode: string, event: Event){
    this.printQueue.push(barcode)
  }
  
  refreshGrid(barcode, event: Event){
    console.log("refreshGrid")
    let el = event.target;
    let icon = this.findChildren(el);
    if (!icon) {
      icon = this.findSibbling(el)
    }
    if (!this.printQueue.includes(barcode)){
      this.printQueue.push(barcode);
      this.renderer.addClass(icon, 'fa-minus');
      this.renderer.addClass(icon, 'text-danger');
      this.renderer.addClass(icon, barcode);
      this.renderer.removeClass(icon, 'fa-plus');
      this.renderer.removeClass(icon, 'text-success');
    }
  }


  addRemovePrintQueue(barcode: string, event: Event) {
    console.log(this.printQueue)
    let el = event.target;
    let icon = this.findChildren(el);
    if (!icon) {
      icon = this.findSibbling(el)
    }

    if (this.printQueue.includes(barcode)){
      var index = this.printQueue.indexOf(barcode)
      this.printQueue.splice(index, 1);
      this.renderer.removeClass(icon, 'fa-minus');
      this.renderer.removeClass(icon, 'text-danger');
      this.renderer.removeClass(icon, barcode);
      this.renderer.addClass(icon, 'fa-plus');
      this.renderer.addClass(icon, 'text-success');
    }else{
      this.printQueue.push(barcode);
      this.renderer.addClass(icon, 'fa-minus');
      this.renderer.addClass(icon, 'text-danger');
      this.renderer.addClass(icon, barcode);
      this.renderer.removeClass(icon, 'fa-plus');
      this.renderer.removeClass(icon, 'text-success');
    }
  }
  
  OpenModalAsignacion(){
    this.modalService.open(AssignComponent, { size: 'lg' })
  } 
  
  OpenModalRebaja(){
    this.modalService.open(UpdateComponent, { size: 'lg' });
  }
  
  openPrint(){
    this.printModalInstance = this.modalService.open(this.printWO)
  }

  printAll(){
    if (this.printQueue.length > 0) {
      window.open(environment.server_ip + 'wo/print/?barcodes=' + this.printQueue);
      this.clearAllQueue();
      this.printModalInstance.close();
    } else {
      this.toastrService.warning('No hay para imprimir');
    }
  }

  clearAllQueue(){
    this.printQueue = [];
    this.printModalInstance.close();
  }

  createButton(idClient:any, event?:Event){
    console.log(idClient)
    if(event){
      event.stopPropagation();
    }
    this.currentButtons.push(idClient);
    console.log(this.currentButtons)
    this.fireUpdates();
    this.listAll();
    localStorage.setItem("buttons", this.currentButtons.toString())
  }
  
  removeButton(id: number){
    console.log(this.currentButtons)
    this.currentButtons.splice(
      this.currentButtons.indexOf(id),
      1
    )
    console.log(this.currentButtons)
    
    // this.setPage({ offset: 0 }, true);
    this.fireUpdates();
    this.listAll();
    localStorage.setItem("buttons", this.currentButtons.toString())
  } 

  filterTable(id: number){
    this.filteringArgs = {
      company: id,
      excluded_companies__in: ""
    }
    this.filters.patchValue(this.filteringArgs)
    this.setPage({ offset: 0 }, true);
  }

  // listcompanie(){
  //   this.filteringArgs = {
  //     company:"",
  //     excluded_companies__in: this.currentButtons.toString()
  //   }
  //   this.filters.patchValue(this.filteringArgs)
  //   // this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
  //   //     dtInstance.ajax.reload();
  //   // }); 
  //   this.setPage({ offset: 0 }, true);
  // }

  listAll(){
    this.filteringArgs = {
      // company:"",
      excluded_companies__in: this.currentButtons.toString()
    }
    this.filters.patchValue(this.filteringArgs)
    this.setPage({ offset: 0 }, true);
    console.log(this.filters.value)
        // this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        //     dtInstance.ajax.reload();
        // }); 
  }

  fila(num){
    console.log(num);
    this.filters.patchValue({limit:num})
    this.setPage({ offset: 0 }, true);
  }

  ngOnDestroy(){
    this.dashSock.complete();
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }
}
