import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FixAddressComponent } from './fix-address.component';

describe('FixAddressComponent', () => {
  let component: FixAddressComponent;
  let fixture: ComponentFixture<FixAddressComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FixAddressComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FixAddressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
