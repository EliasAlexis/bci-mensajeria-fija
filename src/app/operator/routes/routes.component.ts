import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LayoutService } from '../../layouts/services/layout.service';
import * as L from 'leaflet';
import { MapsAPILoader } from '@agm/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FixAddressComponent } from './fix-address/fix-address.component';
import { ToastrService } from 'ngx-toastr'
import { Subject } from 'rxjs'
import { takeUntil } from 'rxjs/operators'
import domtoimage from 'dom-to-image';

@Component({
  selector: 'app-routes',
  templateUrl: './routes.component.html',
  styleUrls: [
    './routes.component.scss',
    '../../layouts/containers/full/full.component.scss',
  ]
})
export class RoutesComponent implements OnInit, OnDestroy {
  private map;
  private waypoints = [];
  invalid_wos = [];
  wos = [];
  routeLookups: any;
  isLoading: boolean = false;
  alreadyGenerated: boolean = false;
  private generatedRoute: any;
  private destroy$: Subject<boolean> = new Subject<boolean>();
  dtOptions = {
    language: {
      url: '../../../assets/languages/Spanish.json'
    },
    searching: false,
  }

  private icon = new L.Icon({
    iconSize: [25, 41],
    iconAnchor: [13, 41],
    iconUrl: 'assets/marker-icon.png',
    shadowUrl: 'assets/marker-shadow.png'
  })
  constructor(
    private layoutService: LayoutService,
    private mapsAPILoader: MapsAPILoader,
    private modalService: NgbModal,
    private toastrService: ToastrService,
    private activatedRoute: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.routeLookups = this.activatedRoute.snapshot.queryParams;
    // let today = new Date();
    let lookupDate: Date;
    if (isNaN(Date.parse(this.routeLookups.log_date + 'T00:00:00'))){
      lookupDate = new Date();
    }else{
      lookupDate = new Date(Date.parse(this.routeLookups.log_date + 'T00:00:00'));
    }
    // if (today.toDateString() != lookupDate.toDateString()){
    //   this.alreadyGenerated = true;
    // }
    if(navigator.geolocation){
      navigator.geolocation.getCurrentPosition(
        (position: Position) => {
          this.drawMap(position.coords.latitude, position.coords.longitude)
        },
        err => {
          this.drawMap(0, 0)
        }
      )
    }
  }
  
  ngAfterViewInit(){
    if (this.routeLookups.type == 'auto'){
      setTimeout(async () => {
        // await this.calcRoute();
        await this.saveRoute();
      }, 500)
    }
  }

  adjustAddress(wo:any){
    let comps: any = {
      lat: '',
      lng: '',
      address: ''
    }
    if (wo.current_step.code == 2 || wo.current_step.to_sender) {
      comps.lat = wo.sender_lat;
      comps.lng = wo.sender_lng;
      comps.address = `${wo.sender_address} ${wo.sender_address_number}, ${wo.sender_comune}`
    } else {
      comps.lat = wo.receiver_lat;
      comps.lng = wo.receiver_lng;
      comps.address = `${wo.receiver_address} ${wo.receiver_address_number}, ${wo.receiver_comune}`
    }
    return comps
  }
  
  drawMap(lat:number, lng:number){
    this.map = L.map('map', {
      center: [lat, lng],
      zoom: 13,
      zoomControl: false,
    })
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 19,
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    }).addTo(this.map)

    this.layoutService.getLogs({ ...this.routeLookups, limit: 999 })
      .pipe(
        takeUntil(this.destroy$)
      )
      .subscribe(
        (res: any) => {
          let wos = res.results.map(el => el.work_order)
          this.wos = wos.filter(el => {
            if (parseInt(el.sender_lat) == 0) {
              el.invalid_type = 'sender'
              this.invalid_wos.push(el)
              return false
            } else if (parseInt(el.receiver_lat) == 0) {
              el.invalid_type = 'receiver'
              this.invalid_wos.push(el)
              return false
            } else {
              return true;
            }
          })
          this.drawMarkers()
        }
      )
  }

  drawMarkers(){
    let markers = L.featureGroup();
    for (let el of this.wos) {
      let comps = this.adjustAddress(el);
      let woType: string;
      if (el.current_step.code == 5 || el.current_step.to_sender) {
        woType = 'Entrega'
      } else if (el.current_step.code == 2) {
        woType = 'Retiro'
      } else {
        woType = el.current_step.name;
      }
      markers.addLayer(
        L.marker([comps.lat, comps.lng], { icon: this.icon })
        .bindTooltip(`<b>${el.barcode}</b> (${woType}) </br>${comps.address}`, { permanent: true, direction: 'bottom' })
      )
      this.waypoints.push(
        {
          lat: parseFloat(comps.lat),
          lng: parseFloat(comps.lng)
        }
      )
    }
    markers.addLayer(
      L.marker([-33.438436, -70.6277667], { icon: this.icon,  })
      .bindTooltip(`Conexxion`, { permanent: true, direction: 'bottom'})
    )
    markers.addTo(this.map)
    this.map.fitBounds(
      markers.getBounds().pad(0.1)
    )
  }

  async calcRoute(){
    if(this.invalid_wos.length > 0){
      let a = confirm('Hay OTs con direcciones inválidas, si no son corregidas quedarán fuera del cáculo de rutas. ¿Desea continuar?');
      if(!a){
        return
      }
    }
    this.isLoading = true;
    await this.mapsAPILoader.load().then( async () => {
      let waypoints = this.waypoints.map( el => {
        return {
          location: new google.maps.LatLng({
            lat: el.lat,
            lng: el.lng
          })
        }
      })
      let directions = new google.maps.DirectionsService;
      await new Promise((resolve, reject) => {
        directions.route(
          {
            destination: { lat: -33.438436, lng: -70.6277667 },
            origin: { lat: -33.438436, lng: -70.6277667 },
            optimizeWaypoints: true,
            travelMode: google.maps.TravelMode.DRIVING,
            waypoints: waypoints
          },
          (result, status)=>{
            let points = [];
            for (let el of result.routes[0].overview_path){
              points.push([el.lat(), el.lng()])
            }
            L.polyline(
              points
            ).on('add', () => {
                resolve()
              })
              .addTo(this.map)
            this.isLoading = false;
            this.generatedRoute = {
              transporter: this.routeLookups.transporter,
              route_polyline: result.routes[0].overview_polyline,
              orders: result.routes[0].waypoint_order
            }
          }
        )
      })
    })
  }

  fixWO(index:number){
    const annonModal = this.modalService.open(
      FixAddressComponent,
      { 
        size: 'lg',
        keyboard: false
      }
    )
    annonModal.componentInstance.wo = this.invalid_wos[index];
    annonModal.result
      .then(
        (wo : any)=>{
          this.invalid_wos.splice(index, 1)
          this.wos.push(wo);
          let comps = this.adjustAddress(wo);
          let woType:string;
          if (wo.current_step.code == 5 || wo.current_step.to_sender){
            woType = 'Entrega'
          }else if (wo.current_step.code == 2){
            woType = 'Retiro'
          }else{
            woType = wo.current_step.name;
          }
          L.marker([comps.lat, comps.lng], { icon: this.icon })
            .addTo(this.map)
            .bindTooltip(`<b>${wo.barcode}</b> (${woType}) </br>${comps.address}`)
          this.waypoints.push(
            {
              lat: parseFloat(comps.lat),
              lng: parseFloat(comps.lng)
            }
          )
        }
      )
      .catch(
        () => {}
      )
  }

  async saveRoute(){
    this.isLoading = true;
    if(!this.generatedRoute){
      await this.calcRoute()
      // this.toastrService.warning('Debe generar la ruta antes de guardarla.')
      // return;
    }
    const dataURL = await domtoimage.toJpeg(this.map._container, { height: this.map._size.y, width: this.map._size.x, quality: 0.95 })
    // let a = confirm('Luego de guardar la ruta no puede ser generada nuevamente o modificada. ¿DESEA CONTINUAR?')
    // if(a){
    this.layoutService.createAssignManifest(
      {
        ...this.routeLookups,
        ...this.generatedRoute,
        map_picture: dataURL
      }
    )
      .pipe(
        takeUntil(this.destroy$)
      )
      .subscribe(
        res => {
          this.alreadyGenerated = true;
          let objFra = document.createElement('iframe');
          objFra.style.visibility = 'hidden'
          objFra.src = window.URL.createObjectURL(res);
          document.body.appendChild(objFra);
          objFra.contentWindow.focus();
          objFra.contentWindow.print();
        },
        err => {
          this.isLoading = false;
          if (err.status === 500) {
            this.toastrService.error('Ha ocurrido un problema al generar el manifiesto');
          } else {
            for (let errorHead in err.errMsg) {
              this.toastrService.error(err.errMsg[errorHead]);
            }
          }
        }
      )
  // }
  }

  ngOnDestroy(){
    this.destroy$.next();
    this.destroy$.unsubscribe();
  }
}
