import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintFalabellaComponent } from './print-falabella.component';

describe('PrintFalabellaComponent', () => {
  let component: PrintFalabellaComponent;
  let fixture: ComponentFixture<PrintFalabellaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrintFalabellaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintFalabellaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
