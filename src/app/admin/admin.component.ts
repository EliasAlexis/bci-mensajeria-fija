import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin',
  template: '<app-full-layout></app-full-layout>',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
