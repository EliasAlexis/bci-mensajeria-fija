import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Modules
import { LayoutsModule } from '../../layouts/layouts.module';
import { CompaniesRoutingModule } from './companies-routing.module';
import { NgxMaskModule, IConfig } from 'ngx-mask'

// Components
import { EditLiscompanyComponent } from './edit-liscompany/edit-liscompany.component';
import { CompaniesComponent } from './companies.component';
import { CreateCompanyComponent } from './create-company/create-company.component';
import { ListUserComponent } from './list-user/list-user.component';
import { CreateBranchComponent } from './create-branch/create-branch.component';
import { CreateUserComponent } from './create-user/create-user.component';
// import { SolicitudServicioComponent } from './solicitud-servicio/solicitud-servicio.component';
import { FilesListComponent } from './files-list/files-list.component';
// import { FichaComponent } from './ficha/ficha.component';
// import { PipePipe } from './list-user/pipe/pipe.pipe';

import { CompaniesService } from './services/companies.services';

export const options: Partial<IConfig> | (() => Partial<IConfig>) = {}

@NgModule({
  declarations: [CompaniesComponent, EditLiscompanyComponent, CreateCompanyComponent, ListUserComponent, CreateBranchComponent, CreateUserComponent, FilesListComponent],
  imports: [
    CommonModule,
    CompaniesRoutingModule,
    LayoutsModule,
    NgxMaskModule.forRoot(options),
  ],
  // exports:[
    // PipePipe
  // ],
  providers: [
    CompaniesService,
  ]
})
export class CompaniesModule { }
