import { Component, OnInit, OnDestroy } from '@angular/core';
import { CompaniesService } from '../services/companies.services'
import { ListUsers } from '../../models'
import { ToastrService } from "ngx-toastr";
import { Router, ActivatedRoute } from "@angular/router";
import { FormBuilder, Validators } from '@angular/forms';
import { rutValidator } from '../../../extra/validators';
import { Observable } from 'rxjs'
import { map } from 'rxjs/operators'
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { MetaTitleService } from '../../../services/meta-title.service';

const FIELDS = {
  branch: "Sucursal",
  email: "E-mail",
  first_name: "Nombres",
  last_name: "Apellidos",
  phone: "Teléfono",
  rut: "RUT",
  status: "Estado",
  username: "Nombre de Usuario",
}

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.scss']
})
export class CreateUserComponent implements OnInit, OnDestroy {

  listUsersParams: ListUsers = new ListUsers();
  public isNew: boolean;
  public branchesList;
  public isLoading: boolean = false;

  userForm = this.fb.group({
    username: ['', Validators.required],
    first_name: ['', Validators.required],
    last_name: ['', Validators.required],
    rut: ['', [Validators.required, rutValidator(), Validators.minLength(7)]],
    phone: ['', Validators.required],
    email: ['', [Validators.required, Validators.email]],
    group: ['users', [Validators.required]],
    branch: ['', [Validators.required]],
    status: ['active', [Validators.required]],
  })
  destroy$: Subject<boolean> = new Subject<boolean>();
  public userTypes: Observable<any>;

  constructor(
    private companiesService: CompaniesService,
    private toastrService: ToastrService,
    private Router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private metaTitleService: MetaTitleService,
  ) {
    this.metaTitleService.updateTitle();
    this.route.data
      .pipe(
        takeUntil(this.destroy$)
      ).subscribe(
        data => {
          if (data.user === true) {
            this.isNew = true;
          } else {
            this.isNew = false;
            this.userForm.patchValue(data.user);
            this.userForm.patchValue({ branch: data.user.branch.id });
          }
        }
      )
  }

  ngOnInit() {
    this.branchesList = this.companiesService.listBranches(
      this.route.snapshot.params['id'],
      { 
        limit: 999,
        ordering: 'name'
      }
      )
      .pipe(
        map(
          (res: any) => {
            return res.results
          }
        )
      )
    this.userTypes = this.companiesService.retrieveUserTypes()
      .pipe(
        map(
          (response: any[]) => {
            return Array.from(new Set(response.map(a => a.id)))
              .map(id => {
                return response.find(a => a.id === id)
              })
          }
        )
      );
  }

  createUserSave() {
    this.isLoading = true;
    let idCompanie = this.route.snapshot.params['id']
    this.companiesService.createUser(idCompanie, this.userForm.value)
      .pipe(
        takeUntil(this.destroy$)
      ).subscribe(
        (response: any) => {
          this.isLoading = false;
          this.listUsersParams = response.results;
          this.toastrService.success('Datos guardados', '', { timeOut: 6000 });
          this.Router.navigate(['../'], { relativeTo: this.route });
        },
        error => {
          this.isLoading = false;
          if (error.errMsg && error.status != 500) {
            for (let errHead in error.errMsg) {
              this.toastrService.error(error.errMsg[errHead], FIELDS[errHead]);
            }
          } else {
            this.toastrService.error("Ha ocurrido un problema");
          }
        }
      )
  }

  updateUser() {
    this.isLoading = true;
    let idCompnay = this.route.snapshot.params['id'];
    let idUser = this.route.snapshot.params['user_id'];
    this.companiesService.updateUser(idCompnay, idUser, this.userForm.value)
      .pipe(
        takeUntil(this.destroy$)
      ).subscribe((response: any) => {
        this.isLoading = false;
        this.toastrService.success('', 'Cambios realizados correctamente');
        this.Router.navigate(['../'], { relativeTo: this.route });
      },
        error => {
          this.isLoading = false;
          if (error.errMsg && error.status != 500) {
            for (let errHead in error.errMsg) {
              this.toastrService.error(error.errMsg[errHead], FIELDS[errHead]);
            }
          } else {
            this.toastrService.error("Ha ocurrido un problema");
          }
        }
      )
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }
}
