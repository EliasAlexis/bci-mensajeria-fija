import { Component, OnInit, Renderer2,ElementRef, ViewChild } from '@angular/core';
import { EditListCompany, ListBranches, ListUsers } from '../../models';
import { CompaniesService } from '../services/companies.services';
import { ActivatedRoute, Router } from '@angular/router';
import { DataTableDirective } from 'angular-datatables';
import { DownloadService } from '../../../services/download.service';
import { MetaTitleService } from '../../../services/meta-title.service';

const FIELDS =  {
  "name": "Nombre",
  "address": "Dirección",
  "phone": "Teléfono",
  "contact_name": "Contacto",
  "comune": "Comuna",
  "province": "Provincia",
  "region": "Región",
  "max_users": "Usuarios",
  "company": "Empresa",
}

@Component({
  selector: 'app-edit-liscompany',
  templateUrl: './edit-liscompany.component.html',
  styleUrls: ['./edit-liscompany.component.scss']
})
export class EditLiscompanyComponent implements OnInit {
  
  @ViewChild('branchesTable') branchesTable : ElementRef;
  @ViewChild('contentModal') contentModal : ElementRef;
  @ViewChild(DataTableDirective) dtElement : DataTableDirective;

  listBrancheModal: ListBranches = new ListBranches();
  editListCompany: EditListCompany = new EditListCompany();
  ListUsers: ListUsers = new ListUsers;
  public isCollapsed = true;

  constructor(
    private companiesService: CompaniesService,
    private route: ActivatedRoute,
    private router: Router,
    private renderer: Renderer2,
    private downloadService: DownloadService,
    private metaTitleService: MetaTitleService,
  ) { 
        this.metaTitleService.updateTitle();
    let initial_table = {
      dom: 'Bfrtip',
      serverSide: true,
      language: {
        url: '../../../assets/languages/Spanish.json'
      },
      processing: true,
        buttons: {
          buttons:[
            {
               text: 'Excel',
               key: '2',
               className: 'btn btn-success btn-sm text-white',
               action: (e, dt, node, config) => {
                 let dtparams = dt.ajax.params()
                 let params = {
                   report_type: 'excel',
                   search: dtparams.search.value
                 }
                 const company = this.route.snapshot.params["id"]
                 this.downloadService.downloadReport('companies/' + company + '/branches/reports/', params)
                   .subscribe(
                     res => {
                       this.downloadFile('xlsx', res)
                     }
                   )
               }
             },
             {
              text: 'PDF',
              key: '2',
              className: 'btn btn-danger btn-sm text-white',
              action: (e, dt, node, config) => {
                let dtparams = dt.ajax.params()
                let params = {
                  report_type: 'pdf',
                  search: dtparams.search.value
                }
                const company = this.route.snapshot.params["id"]
                this.downloadService.downloadReport('companies/' + company + '/branches/reports/', params)
                  .subscribe(
                    res => {
                      this.downloadFile('pdf', res)
                    }
                  )
              }
            },
           ]
        },
        columns: [
          { 
            title: 'Nombre',
            data: 'name',
            render: function (data: any, type: any, full: any) {
              if(data){
                return data[0].toUpperCase() + data.slice(1);
              }else{
                return data;
              }
            }
          },{ 
            title: 'Direccion',
            data: 'address'
          },{ 
            title: 'Teléfono',
            data: 'phone',
            render: function (data: any, type: any, full: any) {
              if(data){
                return  data.substr(0,2)+' '+data.substr(2,2) +' '+data.substr(4,3) +' '+data.substr(7,4) 
              }else{
                return data
              }
            }
          },{ 
            title: 'Comuna',
            data: 'comune'
          },{ 
            title: 'Region',
            data: 'region'
          },
          { 
            title: 'Editar',
            data: 'id',
            className: 'text-center',
            orderable: false,
            render: function (data: any, type: any, full: any) {
              return `<button type="button" class="btn btn-info btn-sm" idEditBranche="${data}"><i class="fas fa-edit" idEditBranche="${data}"></i>
                    </button>`
            }
          },{ 
            title: 'Usuario',
            data: 'id',
            className: 'text-center',
            orderable: false,
            render: function (data: any, type: any, full: any) {
              return  `<button type="button" idBrancheRedirectUser="${data}" class="btn btn-primary btn-sm" ><i idBrancheRedirectUser="${data}" class="fa fa-users"></i></button>`
          }
          },
        ],
        ajax : (data : any, callback ) => {
          let order = data.order[0];
          let consulta: any = {
            search: data.search.value,
            offset: data.start,
            limit: data.length,
            ordering: (order.dir == 'desc' ? '-' : '') + data.columns[order.column].data
          };
          this.companiesService.listBranches(this.route.snapshot.params['id'], consulta)
            .subscribe(
              (response: any) => {
                callback({
                  recordsTotal: response.count,
                  recordsFiltered: response.count,
                  data: response.results
                });
              },
              error => {
                console.log("error");
                console.log(error);
              }
            )
        },  
    };
    this.route.data.subscribe(
      (data: any) => {
        this.editListCompany = data.company;
        if (!this.editListCompany.trader){
          this.editListCompany.trader = new ListUsers();
        }
      }
    )

    this.route.parent.data.subscribe(
      (data: any) => {
        if(data.group != "dXNlcnM="){
          initial_table.buttons.buttons.unshift(
            {
              text: 'Crear',
              key: '1',
              className: 'btn btn-primary btn-sm',
              action: (e, dt, node, config) => {
              this.router.navigate(['./new'],{relativeTo : this.route});
              }
            }
          )
        }
        this.dtOptions = initial_table;
      }
    )
  }

  ngOnInit() {
  }

  dtOptions: any = {}
    
  ngAfterViewInit(): void {
    this.renderer.listen(this.branchesTable.nativeElement, 'click', (event) => {
      if (event.target.hasAttribute("idEditBranche")) {
        this.router.navigate(
          [event.target.getAttribute("idEditBranche")],
          { relativeTo: this.route }
        );
      }
      if (event.target.hasAttribute("idBrancheRedirectUser")) {
        this.router.navigate(
          [event.target.getAttribute("idBrancheRedirectUser"), 'users'],
          { relativeTo: this.route }
        );
      }
    });
  }

  downloadFile(extension: string, res: Blob): void {
    const now = new Date();
    let anchor = document.createElement('a');
    anchor.download = "Sucursales " + now.toLocaleString('es-CL') + "." + extension;
    anchor.href = window.URL.createObjectURL(res);
    anchor.dataset.downloadurl = ['text/plain', anchor.download, anchor.href].join(':');
    anchor.click();
  }

}