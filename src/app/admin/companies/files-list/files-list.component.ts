import { Component, OnInit, ViewChild, ElementRef, Renderer2 } from '@angular/core';
import { AdmService } from '../../services/adm.service';
import { Router, ActivatedRoute } from "@angular/router";
import { DownloadService } from '../../../services/download.service';
import { MetaTitleService } from '../../../services/meta-title.service';

@Component({
  selector: 'app-files-list',
  templateUrl: './files-list.component.html',
  styleUrls: ['./files-list.component.scss']
})
export class FilesListComponent implements OnInit {
  @ViewChild('companyFilesTable') companyFilesTable : ElementRef;

  dtOptions: any = {
    dom: 'Bfrtip',
    serverSide: true,
    processing: true,
    language: {
      url: '../../../assets/languages/Spanish.json'
    },
    buttons: {
      buttons: [
        {
          text: 'Crear',
          key: '1',
          className: 'btn btn-primary btn-sm',
          action: (e, dt, node, config) => {
            this.router.navigate(['./new'], { relativeTo: this.route });
          }
        },{
          text: 'Excel',
          key: '2',
          className: 'btn btn-success btn-sm text-white',
          action: (e, dt, node, config) => {
            let dtparams = dt.ajax.params()
            let params = {
              report_type: 'excel',
              search: dtparams.search.value
            }
            const company = this.route.snapshot.params["id"]
            this.downloadService.downloadReport('settings/reports/', params)
              .subscribe(
                res => {
                  this.downloadFile('xlsx', res)
                }
              )
          }
        },{
          text: 'PDF',
          key: '2',
          className: 'btn btn-danger btn-sm text-white',
          action: (e, dt, node, config) => {
            let dtparams = dt.ajax.params()
            let params = {
              report_type: 'pdf',
              search: dtparams.search.value
            }
            const company = this.route.snapshot.params["id"]
            this.downloadService.downloadReport('settings/reports/', params)
              .subscribe(
                res => {
                  this.downloadFile('pdf', res)
                }
              )
          }
        },
      ]
    },
    columns: [
      {
        title: 'Fecha creación',
        data: 'log',
        render: function (data: any, type: any, full: any) {
          return data[0] ? data[0].stamp : "";
        }
      }, {
        title: 'Razón social',
        data: 'company_name',
        render: function (data: any, type: any, full: any) {
          if(data){
            return data[0].toUpperCase() + data.slice(1);
          }else{
            return data;
          }
        }
      }, {
        title: 'Nombre Fantasía',
        data: 'commercial_business',
        render: function (data: any, type: any, full: any) {
          if(data){
            return data[0].toUpperCase() + data.slice(1);
          }else{
            return data;
          }
        }
      }, {
        title: 'Ejecutivo',
        data: 'owner.first_name'
      }, {
        title: 'Responsable',
        data: null,
        render: function (data: any, type: any, full: any) {
          return `${data.owner.first_name} ${data.owner.last_name}`
        }
      }, {
        title: 'Estado',
        data: 'status',
        render: function (data: any, type: any, full: any) {
          switch(data){
            case "draft":
              return "Borrador";
              break;
            case "sent":
              return "Pendiente";
              break;
            case "rejected":
              return "Rechazada";
              break;
            case "accepted":
              return "Aprobada";
              break;
          }
        }
      }, {
        title: '',
        data: 'id',
        orderable: false,
        className: 'text-center',
        render: function (data: any, type: any, full: any) {
          return `<button type="button" class="btn btn-info btn-sm" view-file-id="${data}">
                    <i class="fas fa-eye" view-file-id="${data}"></i>
                  </button>`;
        }
      },
    ],
    ajax: (data: any, callback) => {
      let order = data.order[0];
      let consulta: any = {
        search: data.search.value,
        offset: data.start,
        limit: data.length,
        status__in: 'sent,rejected,accepted',
        ordering: (order.dir == 'desc' ? '-' : '') + data.columns[order.column].data
      };
      this.admService.listAvailableFiles(consulta)
        .subscribe(
          (response: any) => {
            console.log(response);
            callback({
              recordsTotal: response.count,
              recordsFiltered: response.count,
              data: response.results
            });
          },
          err => {
            console.log(err)
          }
        )
    },
  };

  constructor(
    private admService: AdmService,
    private renderer: Renderer2,
    private route: ActivatedRoute,
    private router: Router,
    private downloadService: DownloadService,
    private metaTitleService: MetaTitleService,
  ) {
    this.metaTitleService.updateTitle();
  }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    this.renderer.listen(this.companyFilesTable.nativeElement, 'click', (event) => {
      if (event.target.hasAttribute("view-file-id")) {
        this.router.navigate([event.target.getAttribute("view-file-id")], { relativeTo: this.route });
      }
    });
  }

  downloadFile(extension: string, res: Blob): void {
    const now = new Date();
    let anchor = document.createElement('a');
    anchor.download = "Fichas " + now.toLocaleString('es-CL') + "." + extension;
    anchor.href = window.URL.createObjectURL(res);
    anchor.dataset.downloadurl = ['text/plain', anchor.download, anchor.href].join(':');
    anchor.click();
  }

}
