import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin.component';

import { RouteGuard } from '../services/route.guard';
import { PreComponent } from './billing/pre/pre.component';

const routes: Routes = [
   {
    path: '',
    redirectTo: 'companies',
    pathMatch: 'full'
  },
  {
    path: '',
    component: AdminComponent,
    children: [
      {
        path: 'companies',
        loadChildren: () => import('./companies/companies.module').then(m => m.CompaniesModule),
        data: { 
          segment: '/adm/companies',
          group: 'YWRtaW5z'
        },
        canLoad: [RouteGuard],
      },
      {
        path: 'settings',
        loadChildren: () => import('./settings/settings.module').then(m => m.SettingsModule),
        data: { segment: '/adm/settings' },
        canLoad: [RouteGuard],
      },
      {
        path: 'pre',
        component: PreComponent,
        data: { 
          segment: '/adm/pre',
          title: 'Prefacturación'
        },
        canActivate: [RouteGuard],
      }
    ]
  }
  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [
    RouteGuard,
  ]
})
export class AdminRoutingModule { }
