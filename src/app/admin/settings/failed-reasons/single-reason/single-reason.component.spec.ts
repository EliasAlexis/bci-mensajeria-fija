import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleReasonComponent } from './single-reason.component';

describe('SingleReasonComponent', () => {
  let component: SingleReasonComponent;
  let fixture: ComponentFixture<SingleReasonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleReasonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleReasonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
