import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject, Observable, BehaviorSubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { SettingsService } from '../../../services/settings.service';
import { ToastrService } from 'ngx-toastr'
import { MetaTitleService } from '../../../../services/meta-title.service';
import { distinctUntilChanged, debounceTime, flatMap, map } from 'rxjs/operators';
import { AdmService } from '../../../services/adm.service';

const errTitles = {
  reason: "Razón",
  default: "Predeterminada"
}

@Component({
  selector: 'app-single-reason',
  templateUrl: './single-reason.component.html',
  styleUrls: ['./single-reason.component.scss']
})
export class SingleReasonComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<boolean>();
  private reasonId: Number;
  isLoading = false;
  public companies: Observable<any>;
  searchTerm: BehaviorSubject<string> = new BehaviorSubject<string>("");

  reasonForm = this.fb.group({
    reason: ['', Validators.required],
    default: ['yes', Validators.required],
    failed_reason_rel_reason: ['', Validators.required],
  })

  constructor(
    private fb: FormBuilder,
    private settingsService: SettingsService,
    private router: Router,
    private route: ActivatedRoute,
    private toast: ToastrService,
    private metaTitleService: MetaTitleService,
    private admService: AdmService,
  ) {
    this.metaTitleService.updateTitle();
    this.route.data
      .subscribe(
        (data: any) => {
          if (data.reason !== 'new') {
            this.reasonId = data.reason.id;
            this.reasonForm.patchValue(data.reason);
            if (data.reason.default == 'yes') {
              this.reasonForm.controls['failed_reason_rel_reason'].disable()
            }
          }else{
            this.reasonForm.controls['failed_reason_rel_reason'].disable()
          }
        }
      )
  }

  ngOnInit(): void {
    this.companies = this.searchTerm
      .pipe(
        distinctUntilChanged(),
        debounceTime(400),
        flatMap(
          term => {
            return this.admService.listcompanyParams({ search: term, limit: 999 })
              .pipe(
                map(
                  (res: any) => {
                    return res.results;
                  }
                )
              )
          }
        )
      )

    this.reasonForm.controls['default'].valueChanges
    .pipe(
      takeUntil(this.destroy$)
    )
    .subscribe(
      val => {
        if(val == 'yes'){
          this.reasonForm.controls['failed_reason_rel_reason'].disable()
        }else{
          this.reasonForm.controls['failed_reason_rel_reason'].enable()
        }
      }
    )
  }

  handleSave() {
    this.isLoading = false;
    this.toast.success("Razón de Fallido guardado exitosamente");
    this.router.navigate(['../'], { relativeTo: this.route });
  }
  private handleErr(err) {
    this.isLoading = false;
    if (err.status === 500) {
      this.toast.error("Ha ocurrido un problema");
    } else {
      for (let errorHead in err.errMsg) {
        this.toast.error(err.errMsg[errorHead], errTitles[errorHead]);
      }
    }
  }

  saveOrCreate() {
    this.isLoading = true;
    let fixedData = {...this.reasonForm.value};
    if (fixedData.failed_reason_rel_reason){
      fixedData.failed_reason_rel_reason = fixedData.failed_reason_rel_reason.map(el => { return  {"company" : el}})
    }
    if (this.reasonId) {
      this.settingsService.modifyReason(this.reasonId, fixedData)
        .pipe(
          takeUntil(this.destroy$)
        )
        .subscribe(
          () => this.handleSave(),
          err => this.handleErr(err)
        )
    } else {
      this.settingsService.createReason(fixedData)
        .pipe(
          takeUntil(this.destroy$)
        )
        .subscribe(
          () => this.handleSave(),
          err => this.handleErr(err)
        )
    }
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }
}
