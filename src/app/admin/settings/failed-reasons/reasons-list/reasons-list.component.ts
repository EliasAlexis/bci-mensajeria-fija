import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { SettingsService } from '../../../services/settings.service';
import { MetaTitleService } from '../../../../services/meta-title.service';
import { ToastrService } from 'ngx-toastr'
import { DataTableDirective } from 'angular-datatables';

@Component({
  selector: 'app-reasons-list',
  templateUrl: './reasons-list.component.html',
  styleUrls: ['./reasons-list.component.scss']
})
export class ReasonsListComponent implements OnInit, OnDestroy {
  @ViewChild(DataTableDirective, { static: false }) dtElement: DataTableDirective;

  private destroy$: Subject<boolean> = new Subject<boolean>();
  currentPage: Array<any> = [];

  dtOptions: any = {
    dom: 'Bfrtip',
    //serverSide: true,
    //processing: true,
    language: {
      url: '../../../assets/languages/Spanish.json'
    },
    buttons: {
      buttons: [
        {
          text: 'Crear',
          key: '1',
          className: 'btn btn-primary btn-sm',
          action: (e, dt, node, config) => {
            this.router.navigate(['./new'], { relativeTo: this.route });
          }
        },
      ]
    },
    columns: [
      {
        data: 'reason',
      },
      {
        data: 'default_display',
      },
      {
        data: 'id',
        orderable: false
      },
    ],
    ajax: (data: any, callback) => {
      // let order = data.order[0];
      // let filters: any = {
      //   search: data.search.value,
      //   offset: data.start,
      //   limit: data.length,
      //   ordering: (order.dir == 'desc' ? '-' : '') + data.columns[order.column].data,
      // };
      this.settingsService.listReasons({})
        .pipe(
          takeUntil(this.destroy$)
        )
        .subscribe(
          (response: any) => {
            this.currentPage = response;
            callback({
              recordsTotal: response.length,
              recordsFiltered: response.length,
              data: []
            });
          },
          err => {
            console.log(err)
          }
        )
    },
  };

  constructor(
    private settingsService: SettingsService,
    private router: Router,
    private route: ActivatedRoute,
    private metaTitleService: MetaTitleService,
    private toast: ToastrService,
  ) {
    this.metaTitleService.updateTitle();
  }

  ngOnInit(): void {
  }

  delReason(el:number){
    let a = confirm('¿Está seguro de eliminar la razón de fallido?');
    if(!a){
      return;
    }
    this.settingsService.deleteReason(el)
      .pipe(
        takeUntil(this.destroy$)
      )
      .subscribe(
        res => {
          this.toast.success('Razón de fallido eliminada correctamente');
          this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            // Destroy the table first
            dtInstance.ajax.reload();
          });
        },
        err => {
          this.toast.error('No es posible eliminar la razón de fallido')
        }
      )
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

}
