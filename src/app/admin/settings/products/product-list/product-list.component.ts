import { Component, OnInit, ViewChild, ElementRef, Renderer2 } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { SettingsService } from '../../../services/settings.service';
import { MetaTitleService } from '../../../../services/meta-title.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {
  @ViewChild('productsTable') productsTable : ElementRef;

  dtOptions: any = {
    dom: 'Bfrtip',
    serverSide: true,
    processing: true,
    language: {
      url: '../../../assets/languages/Spanish.json'
    },
    buttons: {
      buttons: [
        {
          text: 'Crear',
          key: '1',
          className: 'btn btn-primary btn-sm',
          action: (e, dt, node, config) => {
            this.router.navigate(['./new'], { relativeTo: this.route });
          }
        },
      ]
    },
    columns: [
      {
        title: 'Nombre',
        data: 'name',
      }, {
        title: 'Estado',
        data: 'status',
        render: function (data: any, type: any, full: any) {
          switch(data){
            case "active":
              return "Activo";
              break;
            case "disabled":
              return "Desactivado";
              break;
          }
        }
      }, {
        title: '',
        data: 'id',
        orderable: false,
        className: 'text-center',
        render: function (data: any, type: any, full: any) {
          return `<button type="button" class="btn btn-info btn-sm" view-file-id="${data}">
                    <i class="fas fa-eye" view-file-id="${data}"></i>
                  </button>`;
        }
      },
    ],
    ajax: (data: any, callback) => {
      let order = data.order[0];
      let filters: any = {
        search: data.search.value,
        offset: data.start,
        limit: data.length,
        ordering: (order.dir == 'desc' ? '-' : '') + data.columns[order.column].data
      };
      this.settingsService.listProducts(filters)
        .subscribe(
          (response: any) => {
            callback({
              recordsTotal: response.count,
              recordsFiltered: response.count,
              data: response.results
            });
          },
          err => {
            console.log(err)
          }
        )
    },
  };

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private settingsService: SettingsService,
    private renderer: Renderer2,
    private metaTitleService: MetaTitleService,
  ) {
    this.metaTitleService.updateTitle();
  }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    this.renderer.listen(this.productsTable.nativeElement, 'click', (event) => {
      if (event.target.hasAttribute("view-file-id")) {
        this.router.navigate([event.target.getAttribute("view-file-id")], { relativeTo: this.route });
      }
    });
  }
}
