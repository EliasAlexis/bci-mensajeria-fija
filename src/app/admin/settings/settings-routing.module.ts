import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ServicesListComponent } from './services/services-list/services-list.component';
import { SingleServiceComponent } from './services/single-service/single-service.component';
import { SegmentsListComponent } from './segments/segments-list/segments-list.component';
import { SingleSegmentComponent } from './segments/single-segment/single-segment.component';
import { ProductListComponent } from './products/product-list/product-list.component';
import { SingleProductComponent } from './products/single-product/single-product.component';
import { TransportListComponent } from './transports/transport-list/transport-list.component';
import { SingleTransportComponent } from './transports/single-transport/single-transport.component';
import { ReasonsListComponent } from './failed-reasons/reasons-list/reasons-list.component';
import { SingleReasonComponent } from './failed-reasons/single-reason/single-reason.component';

import { ServiceResolver, SegmentResolver, ProductResolver, TransportResolver, ReasonResolver } from '../services/settings.resolvers';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'services',
        pathMatch: 'full'
    },
    {
        path: 'services',
        component: ServicesListComponent,
        data: {
            title: 'Listado de servicios'
        }
    }, {
        path: 'services/:id',
        component: SingleServiceComponent,
        resolve: { service: ServiceResolver },
        data: {
            title: 'Servicio'
        }
    },
    {
        path: 'segments',
        component: SegmentsListComponent,
        data: {
            title: 'Listado de tramos'
        }
    }, {
        path: 'segments/:id',
        component: SingleSegmentComponent,
        resolve: { service: SegmentResolver },
        data: {
            title: 'Tramo'
        }
    },{
        path: 'products',
        component: ProductListComponent,
        data: {
            title: 'Listado de productos'
        }
    },{
        path: 'products/:id',
        component: SingleProductComponent,
        resolve: { product: ProductResolver },
        data: {
            title: 'Producto'
        }
    },{
        path: 'transports',
        component: TransportListComponent,
        data: {
            title: 'Listado de Vehículos'
        }
    },{
        path: 'transports/:id',
        component: SingleTransportComponent,
        resolve: { transport: TransportResolver },
        data: {
            title: 'Vehículo'
        }
    }, {
        path: 'reasons',
        component: ReasonsListComponent,
        data: {
            title: 'Listado de Razones de Fallidos'
        }
    }, {
        path: 'reasons/:id',
        component: SingleReasonComponent,
        resolve: { reason: ReasonResolver },
        data: {
            title: 'Razón'
        }
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    providers: [ServiceResolver, SegmentResolver, ProductResolver, TransportResolver, ReasonResolver ]
})
export class SettingsRoutingModule { }