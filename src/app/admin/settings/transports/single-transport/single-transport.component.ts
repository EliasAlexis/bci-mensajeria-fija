import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { SettingsService } from '../../../services/settings.service';
import { ToastrService } from 'ngx-toastr'
import { MetaTitleService } from '../../../../services/meta-title.service';

const errTitles = {
  plate: "Patente",
  type : "Tipo",
  available: "Disponible"
}
@Component({
  selector: 'app-single-transport',
  templateUrl: './single-transport.component.html',
  styleUrls: ['./single-transport.component.scss']
})
export class SingleTransportComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<boolean>();
  private transportId: Number;
  isLoading = false;

  transportForm = this.fb.group({
    plate: ['', Validators.required],
    type: ['moto', Validators.required],
    available: ['yes', Validators.required],
  })

  constructor(
    private fb: FormBuilder,
    private settingsService: SettingsService,
    private router: Router,
    private route: ActivatedRoute,
    private toast: ToastrService,
    private metaTitleService: MetaTitleService,
  ) {
    this.metaTitleService.updateTitle();
    this.route.data
      .subscribe(
        (data: any) => {
          if (data.transport !== 'new') {
            this.transportId = data.transport.id;
            this.transportForm.patchValue(data.transport);
          }
        }
      )
  }

  ngOnInit(): void {
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  handleSave() {
    this.isLoading = false;
    this.toast.success("Vehículo guardado exitosamente");
    this.router.navigate(['../'], { relativeTo: this.route });
  }
  private handleErr(err) {
    this.isLoading = false;
    if (err.status === 500) {
      this.toast.error("Ha ocurrido un problema");
    } else {
      for (let errorHead in err.errMsg) {
        this.toast.error(err.errMsg[errorHead], errTitles[errorHead]);
      }
    }
  }

  saveOrCreate(){
    this.isLoading = true;
    if (this.transportId) {
      this.settingsService.modifyTransport(this.transportId, this.transportForm.value)
        .pipe(
          takeUntil(this.destroy$)
        )
        .subscribe(
          () => this.handleSave(),
          err => this.handleErr(err)
        )
    } else {
      this.settingsService.createTransport(this.transportForm.value)
        .pipe(
          takeUntil(this.destroy$)
        )
        .subscribe(
          () => this.handleSave(),
          err => this.handleErr(err)
        )
    }
  }
}
