export class ListCompany {
    name : string;
    rut : string;
    company_type : string;
    }

export class EditListCompany {
    id : string;
    name : string;
    rut : string;
    company_type : string;
    max_users : string;
    max_branches : string;
    status : string;
    created_at : string;
    email : string;
    phone : string;
    contact_name : string;
    trader: ListUsers;
    }

export class ListBranches {
    id : string;
    name : string;
    address : string;
    phone : string;
    contact_name : string;
    comune : string;
    province : string;
    region : string;
    max_users : string;
    company : string;
    email : string;
    }

export class ListUsers {
    id : string;
    username : string;
    first_name : string;
    last_name : string;
    rut : string;
    status : string;
    phone : string;
    email : string;
    branch : string;
    RutFormat : string;
    }

export class CreateCompany{
    id?: number;
    name: string;
    rut: string;
    company_type: string = 'client';
    max_users: string;
    max_branches: string;
    status: string = 'active';
    billing_contacts? : Array<BillingInfo>;
    trader: ListUsers;
    }



export class CreateBranch{
    name: string;
    address: string;
    phone: string;
    contact_name: string;
    province: string;
    region: string;
    max_users: string;
    }

export class Segments{
    id: number;
    name: string;
    height: string;
    width: string;
    depth: string;
    min_weight: string;
    max_weight: string;
    pricings?: Array<Pricings>;
}

export class Services{
    id: number;
    name: string;
    status: string;
    pricings?: Array<Pricings>;
}

export class Pricings{
    id: number;
    is_default:string;
    no_return: string;
    with_return: string;
    min_quantity: string;
    segment: string;
    service: string;
    company: string;
}

export class BillingInfo {
    name: string;
    email:string;
    phone?:string;
    address?:string;
}

export class CreateFile{
    company_name: string;
    commercial_business : string;
    rut: string;
    address: string;
    phone: string;
    contact_first_name: string;
    contact_last_name: string;
    contact_email: string;
    comune: string;
    province: string;
    region: string;
    billing_contacts: BillingInfo [] = new Array <BillingInfo>() ;
    file_pricings: any;
    status?: string;
}

export class Products {
    id?: number;
    name: string;
    status: string;
}

export class Transports {
    id?: number;
    plate: string;
    type: string;
    available: string;
}
