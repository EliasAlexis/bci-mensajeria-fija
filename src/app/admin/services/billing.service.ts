import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable()
export class BillingService {

  constructor(
    private http: HttpClient,
  ) { }

  getBillingInfo(params: any, toFile?:boolean) {
    let settings: any = { params: params }
    if (toFile == true){
      settings.responseType = 'blob';
    }
    return this.http.get(environment.server_ip + 'billing/pre/', settings )
  }

  getWorkOrders(params: any){
    return this.http.get(environment.server_ip + 'wo/', { params: params } )
  }
}
