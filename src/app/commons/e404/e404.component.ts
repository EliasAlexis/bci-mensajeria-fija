import { Component, OnInit, HostListener } from '@angular/core';
import { AuthService } from '../../auth/services/auth.service';
import { MetaTitleService } from '../../services/meta-title.service';
@Component({
  selector: 'app-e404',
  templateUrl: './e404.component.html',
  styleUrls: ['./e404.component.scss']
})
export class E404Component implements OnInit {

  public innerWidth: any;
  public defaultSidebar: any;
  public showMobileMenu = false;
  public expandLogo = false;
  public sidebartype = 'full';
  public sideElems: any;
  public placeGhost = [];

  Logo() {
    this.expandLogo = !this.expandLogo;
  }

  ngOnInit() {
  }

  constructor(
    private authService: AuthService,
    private metaTitleService: MetaTitleService,
  ) {
      this.metaTitleService.updateTitle();
   }

  logout() {
    this.authService.logout();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.handleSidebar();
  }

  handleSidebar() {
    this.innerWidth = window.innerWidth;
    if (this.innerWidth < 1170) {
      this.sidebartype = 'mini-sidebar';
    } else {
      this.sidebartype = this.defaultSidebar;
    }
  }

  toggleSidebarType() {
    switch (this.sidebartype) {
      case 'full':
        this.sidebartype = 'mini-sidebar';
        break;

      case 'mini-sidebar':
        this.sidebartype = 'full';
        break;

      default:
    }
  }
}
