import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ServicesService } from '../services.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DetailComponent } from './detail/detail.component';
import { AnnotationsComponent } from './annotations/annotations.component';
import { ToastrService } from "ngx-toastr";
import { Subject } from 'rxjs'
import { takeUntil } from 'rxjs/operators';
import * as L from 'leaflet';
import { MetaTitleService } from '../../services/meta-title.service';

const icon = new L.Icon({
  iconSize: [25, 41],
  iconAnchor: [13, 41],
  iconUrl: 'assets/marker-icon.png',
  shadowUrl: 'assets/marker-shadow.png'
})

@Component({
  selector: 'app-tracking',
  templateUrl: './tracking.component.html',
  styleUrls: ['./tracking.component.scss']
})
export class TrackingComponent implements OnInit, OnDestroy {
  public wOInfo: any;
  private map;
  private markers: any;

  // initial center position for the map
  private finalStatus = [0, 6, 7, 9];
  boxMsg = "";
  isLoading: boolean = false;
  destroy$: Subject<boolean> = new Subject<boolean>();
  canAnnontate: boolean = false;
  dataDelivey;


  constructor(
    private servicesService: ServicesService,
    private route: ActivatedRoute,
    private modalService: NgbModal,
    private toastrService: ToastrService,
    private metaTitleService: MetaTitleService,
    ) {
      this.metaTitleService.updateTitle();
      this.route.data.subscribe(
        (data:any) => {
          if (data.group === 'b3BlcmF0b3Jz'){
            this.canAnnontate = true;
          }
        }
      )
    }
    
  public activeSteps = [];

  ngOnInit() {
    this.retrieveLog();
  }

  buildMap(lat: number, lng: number, zoom: number){
    setTimeout(() => {
      this.map = L.map('map', { 
        center: [lat, lng],
        zoom: zoom,
        zoomControl: false,
      })
      L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 19,
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
      }).addTo(this.map)
      this.markers = new L.LayerGroup();
      this.markers.addLayer(
        L.marker([lat, lng], { icon: icon })
      )
      .addTo(this.map)
    }, 0)
  }

  retrieveLog(){
    this.activeSteps = [];
    let barcode = this.route.snapshot.queryParams["barcode"];
    this.servicesService.getWOtracking(barcode)
      .pipe(
        takeUntil(this.destroy$)
      )
      .subscribe(
        (res: any) => {
          let filtered_extra = {};
          let exluded = ['print', 'picture', 'signature'];
          for (let el in res.extra_fields) {
            if (exluded.includes(el)) {
              continue;
            }
            filtered_extra[el] = res.extra_fields[el];
          }
          res.extra_fields = filtered_extra;
          this.wOInfo = res;
          let woSteps = 0;
          console.log(res)
          this.dataDelivey = res.log.find(el =>el.transport)
          // switch (res.log[0].status_code) {
          //   case 0:
          //     this.boxMsg = "Anulado";
          //     break;
          //   case 6:
          //     this.boxMsg = "Entregado";
          //     break;
          //   case 7:
          //     this.boxMsg = "Fallido";
          //     break;
          //   case 9:
          //     this.boxMsg = "Cerrado";
          //     break;
          //   default:
          //     this.boxMsg = "En camino";
          // }

          if (this.finalStatus.includes(res.log[0].status_code)) {
            woSteps = res.log.length;
          } else if (this.wOInfo.has_return == 'with_return') {
            woSteps = 8;
          } else {
            woSteps = 6;
          }
          for (let i = 0; i < woSteps; i++) {
            this.activeSteps.push({
              'completed': false,
              'active': false,
            });
          }
          // this.lat = parseFloat(this.wOInfo.log[0].lat);
          // this.lng = parseFloat(this.wOInfo.log[0].lng);
          let activeSteps = res.log.filter(el => {
            return !el.voided
          });
          this.activeSteps.splice(0, activeSteps.length, ...activeSteps.reverse());
          let last = res.log.find(el => !el.voided);
          last.is_last = true;
          this.boxMsg = last.step_detail;
          
          for (let i = 0; i < activeSteps.length; i++) {
            if (i == activeSteps.length - 1) {
              this.activeSteps[i].active = true;
            }
            if (i < activeSteps.length - 1) {
              this.activeSteps[i].completed = true;
            }
            if (i == activeSteps.length - 1 && activeSteps.length == this.activeSteps.length) {
              this.activeSteps[i].completed = true;
              this.activeSteps[i].active = false;
            }
          }
          this.buildMap(this.wOInfo.log[0].lat, this.wOInfo.log[0].lng, 15 )
        }
      )
  }

  openDetail(index: number){
    const modalref = this.modalService.open(DetailComponent, { size: 'lg' });
    modalref.componentInstance.trackDetail = {
      ...this.wOInfo.log[index],
      delivered_to_name: this.wOInfo.delivered_to_name,
      delivered_to_rut: this.wOInfo.delivered_to_rut
    }
  }

  openAnnons(id: number){
    const annonModal = this.modalService.open(AnnotationsComponent, { size: 'lg' })
    annonModal.componentInstance.woPk = id;
  }

  undoState(id: number){
    this.isLoading = true;
    let a = confirm("¿Desea revertir el último estado de la OT?\nESTA OPCIÓN NO SE PUEDE DESHACER");
    if(a){
      this.servicesService.undoLastOp(id)
        .pipe(
          takeUntil(this.destroy$)
        )
        .subscribe(
          () => {
            this.isLoading = false;
            this.retrieveLog();
          },
          (error:any) => {
            this.isLoading = false;
            if (error.status === 500) {
              this.toastrService.error("Ha ocurrido un problema");
            } else {
              for (let errorHead in error.errMsg) {
                this.toastrService.error(error.errMsg[errorHead]);
              }
            }
          }
        )
    }
  }

  updateMap(id: number){
    if (!id) return;
    let el = this.wOInfo.log.find(el => el.id == id)
    this.markers.clearLayers();
    this.markers.addLayer(
      L.marker(
        [
          el.lat,
          el.lng
        ],
         { icon: icon })
    )
      .addTo(this.map)
    this.map.setView(
      [
        el.lat,
        el.lng
      ],
      15
    )
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }
}
