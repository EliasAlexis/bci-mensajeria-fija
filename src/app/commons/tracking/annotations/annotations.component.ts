import { Component, OnInit, Input, OnDestroy, SecurityContext } from '@angular/core';
import { NgForm } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ServicesService } from '../../services.service';
import { Subscription } from 'rxjs'
import { DomSanitizer } from '@angular/platform-browser'
import { ToastrService } from "ngx-toastr";

@Component({
  selector: 'app-annotations',
  templateUrl: './annotations.component.html',
  styleUrls: ['./annotations.component.scss']
})
export class AnnotationsComponent implements OnInit, OnDestroy {
  @Input() woPk:number;
  private sub: Subscription;
  annons:any = [];
  newAnnon:any = {
    note: ""
  }
  isLoading: boolean = false;

  constructor(
    public modal: NgbActiveModal,
    public servicesService: ServicesService,
    private toastrService: ToastrService,
  ) { }

  ngOnInit() {
    this.retrieveAnnons();
  }

  retrieveAnnons(){
    this.isLoading = true;
    this.sub = this.servicesService.getAnnotations(this.woPk)
      .subscribe(
        (res: any) => {
          this.isLoading = false;
          this.annons = res;
        },
        () => {
          this.isLoading = false;
        }
      )
  }
  addAnnon(annonForm: NgForm){
    this.isLoading = true;
    this.servicesService.addAnnotation(this.woPk, this.newAnnon)
      .subscribe(
        res => {
          annonForm.form.reset();
          this.annons.unshift(res)
          this.isLoading = false;
        },
        () => {
          this.isLoading = false;
          this.toastrService.error("No es posible guardar la anotación");
        }
      )
  }

  ngOnDestroy(){
    this.sub.unsubscribe();
  }
}
