import { NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { ViewProfileComponent } from './view-profile/view-profile.component';
import { ProfileRoutingModule } from './commons-routing.module';
import { LayoutsModule } from '../layouts/layouts.module';
import { CommonsComponent } from './commons.component'
import { E404Component } from './e404/e404.component';
import { TrackingComponent } from './tracking/tracking.component';
import { AgmCoreModule } from '@agm/core';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { DetailComponent } from './tracking/detail/detail.component';
import { AnnotationsComponent } from './tracking/annotations/annotations.component';

@NgModule({
  declarations: [
    ViewProfileComponent,
    CommonsComponent,
    E404Component,
    TrackingComponent,
    DetailComponent,
    AnnotationsComponent,
  ],
  imports: [
    CommonModule,
    ProfileRoutingModule,
    LayoutsModule,
    FormsModule,
    ReactiveFormsModule,
    AgmCoreModule,
    NgxSkeletonLoaderModule,
  ],
  exports: [
    TrackingComponent
  ],
  entryComponents: [
    DetailComponent,
    AnnotationsComponent,
  ]
  // providers:[
  //   {
  //     provide: HTTP_INTERCEPTORS,
  //     // useClass: JwtInterceptor,
  //     multi: true
  //   },
  //   AdmService,
  // ]
})
export class CommonsModule { }
