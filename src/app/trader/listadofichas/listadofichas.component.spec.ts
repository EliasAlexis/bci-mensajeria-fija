import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListadofichasComponent } from './listadofichas.component';

describe('ListadofichasComponent', () => {
  let component: ListadofichasComponent;
  let fixture: ComponentFixture<ListadofichasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListadofichasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListadofichasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
