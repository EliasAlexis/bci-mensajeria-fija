import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TraderComponent } from './trader.component';
import { FichaComponent } from './../layouts/shared/ficha/ficha.component'
import { FileResolver } from '../layouts/services/file.resolve';
import { ListadofichasComponent } from './listadofichas/listadofichas.component'
import { WoListComponent } from '../layouts/shared/wo-list/wo-list.component'
import { TrackingComponent } from '../commons/tracking/tracking.component';
import { RouteGuard } from '../services/route.guard';

const routes: Routes = [{
  path: '',
  component: TraderComponent,
  children: [{
    path: 'files',
    component: ListadofichasComponent,
    data: {
      title: 'Ficha Empresas'
    }
  },{
    path: 'files/:id',
    component: FichaComponent,
    resolve: { file: FileResolver },
    data: { 
      group: 'dHJhZGVycw==',
      title: 'Ficha'
    }
    }, {
      path: 'wolist',
      component: WoListComponent,
      data: {
        title: 'Listado de Solicitudes'
      } 
    }, {
      path: 'wolist/tracking',
      component: TrackingComponent,
      data: {
        title: 'Seguimiento'
      }
    }
  ]
},{
  path: 'companies',
  component: TraderComponent,
  loadChildren: () => import('../admin/companies/companies.module').then(m => m.CompaniesModule),
  data: {
    segment: '/trader/companies',
    group: 'dHJhZGVycw=='
  },
  canLoad: [RouteGuard],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [
    FileResolver,
  ]
})
export class TraderRoutingModule { }
