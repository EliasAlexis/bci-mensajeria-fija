import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommonsModule } from '../commons/commons.module';

import { TraderRoutingModule } from './trader-routing.module';
import { LayoutsModule } from '../layouts/layouts.module'; 
import { NgxMaskModule, IConfig } from 'ngx-mask'

// components
import { TraderComponent } from './trader.component';
import { ListadofichasComponent } from './listadofichas/listadofichas.component'
import { TraderService } from './services/trader.service';

export const options: Partial<IConfig> | (() => Partial<IConfig>) = {}

@NgModule({
  declarations: [
    TraderComponent,
    ListadofichasComponent,
  ],
  imports: [
    CommonModule,
    TraderRoutingModule,
    CommonsModule,
    LayoutsModule,
    NgxMaskModule.forRoot(options),
  ],
  providers: [
    TraderService,
  ]
})
export class TraderModule { }
